<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190711100232 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE action (id INT AUTO_INCREMENT NOT NULL, dict_perimetre_recrutement_id INT DEFAULT NULL, dict_modalites_es_id INT DEFAULT NULL, dict_modalites_enseignement_id INT DEFAULT NULL, dict_boolean_id INT DEFAULT NULL, formation_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, rythme_formation VARCHAR(300) NOT NULL, modalites_alternance VARCHAR(3000) NOT NULL, conditions_specifiques VARCHAR(3000) NOT NULL, info_public_vise VARCHAR(255) NOT NULL, duree_indicative VARCHAR(150) NOT NULL, code_modalite_pedagogique VARCHAR(5) DEFAULT NULL, code_public_vise VARCHAR(5) NOT NULL, modalite_pedagogique VARCHAR(200) DEFAULT NULL, frais_restants VARCHAR(200) DEFAULT NULL, langue_formation VARCHAR(2) DEFAULT NULL, modalite_recrutement VARCHAR(3000) DEFAULT NULL, infos_perimetre_recrutement VARCHAR(50) DEFAULT NULL, prix_horaire_ttc VARCHAR(6) DEFAULT NULL, prix_total_ttc VARCHAR(6) NOT NULL, nombre_heures_centre DOUBLE PRECISION DEFAULT NULL, nombre_heures_entreprise DOUBLE PRECISION DEFAULT NULL, nombre_heures_total DOUBLE PRECISION DEFAULT NULL, detail_condition_prise_en_charge VARCHAR(600) DEFAULT NULL, duree_convention DOUBLE PRECISION DEFAULT NULL, restauration VARCHAR(255) DEFAULT NULL, hebergement VARCHAR(255) DEFAULT NULL, transport VARCHAR(255) DEFAULT NULL, acces_handicapes VARCHAR(255) NOT NULL, niveau_entree_obligatoire INT NOT NULL, prise_en_charge_frais_possible INT NOT NULL, conventionnement INT NOT NULL, modalites_enseignement INT NOT NULL, modalites_entree_sorties INT NOT NULL, UNIQUE INDEX UNIQ_47CC8C92E1BF7BE9 (dict_perimetre_recrutement_id), UNIQUE INDEX UNIQ_47CC8C924D0DC4DD (dict_modalites_es_id), UNIQUE INDEX UNIQ_47CC8C92F2133BF5 (dict_modalites_enseignement_id), UNIQUE INDEX UNIQ_47CC8C92EFAE244F (dict_boolean_id), INDEX IDX_47CC8C925200282E (formation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adresse (id INT AUTO_INCREMENT NOT NULL, geolocalisation_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, ligne VARCHAR(50) NOT NULL, code_postal VARCHAR(5) NOT NULL, ville VARCHAR(50) NOT NULL, departement VARCHAR(3) NOT NULL, code_insee_commune VARCHAR(5) NOT NULL, code_insee_canton VARCHAR(5) NOT NULL, region VARCHAR(2) NOT NULL, pays VARCHAR(2) NOT NULL, UNIQUE INDEX UNIQ_C35F08164046BF3D (geolocalisation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adresse_information (id INT AUTO_INCREMENT NOT NULL, action_id INT DEFAULT NULL, adresse_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_E777E16C9D32F035 (action_id), UNIQUE INDEX UNIQ_E777E16C4DE7DC5C (adresse_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adresse_inscription (id INT AUTO_INCREMENT NOT NULL, session_id INT DEFAULT NULL, adresse_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_909E0F39613FECDF (session_id), UNIQUE INDEX UNIQ_909E0F394DE7DC5C (adresse_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE certification (id INT AUTO_INCREMENT NOT NULL, formation_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, code_rncp VARCHAR(6) NOT NULL, code_certifinfo VARCHAR(6) NOT NULL, INDEX IDX_6C3C6D755200282E (formation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fax (id INT AUTO_INCREMENT NOT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, num_tel INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE numtel (id INT AUTO_INCREMENT NOT NULL, portable_id INT DEFAULT NULL, fax_id INT DEFAULT NULL, tel_fixe_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, value VARCHAR(35) NOT NULL, type VARCHAR(20) NOT NULL, INDEX IDX_7434FDF5E2C1A084 (portable_id), INDEX IDX_7434FDF5FCB68748 (fax_id), INDEX IDX_7434FDF5BF245B23 (tel_fixe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE portable (id INT AUTO_INCREMENT NOT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, num_tel INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tel_fixe (id INT AUTO_INCREMENT NOT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, num_tel INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE web (id INT AUTO_INCREMENT NOT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_formation (id INT AUTO_INCREMENT NOT NULL, formation_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_2EB102C45200282E (formation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE coordonnees (id INT AUTO_INCREMENT NOT NULL, organisme_formateur_id INT DEFAULT NULL, contact_formation_id INT DEFAULT NULL, contact_organisme_id INT DEFAULT NULL, adresse_id INT DEFAULT NULL, web_id INT DEFAULT NULL, tel_fixe_id INT DEFAULT NULL, portable_id INT DEFAULT NULL, fax_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, civilite VARCHAR(50) NOT NULL, nom VARCHAR(50) NOT NULL, prenom VARCHAR(50) NOT NULL, ligne VARCHAR(50) NOT NULL, couriel VARCHAR(160) NOT NULL, UNIQUE INDEX UNIQ_BC8EC7A64625CC6 (organisme_formateur_id), UNIQUE INDEX UNIQ_BC8EC7A7EB71A1A (contact_formation_id), UNIQUE INDEX UNIQ_BC8EC7A716A0AC1 (contact_organisme_id), UNIQUE INDEX UNIQ_BC8EC7A4DE7DC5C (adresse_id), UNIQUE INDEX UNIQ_BC8EC7AFE18474D (web_id), INDEX IDX_BC8EC7ABF245B23 (tel_fixe_id), INDEX IDX_BC8EC7AE2C1A084 (portable_id), INDEX IDX_BC8EC7AFCB68748 (fax_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE date_information (id INT AUTO_INCREMENT NOT NULL, action_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, date DATE NOT NULL, UNIQUE INDEX UNIQ_16002D5F9D32F035 (action_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE domaine_formation (id INT AUTO_INCREMENT NOT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, code_formacode TINYTEXT NOT NULL COMMENT \'(DC2Type:array)\', code_nsf TINYTEXT NOT NULL COMMENT \'(DC2Type:array)\', code_rome VARCHAR(5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE formation (id INT AUTO_INCREMENT NOT NULL, ais_id INT DEFAULT NULL, type_parcours_id INT DEFAULT NULL, type_positionnement_id INT DEFAULT NULL, niveaux_id INT DEFAULT NULL, organisme_formation_responsable_id INT DEFAULT NULL, domaine_formation_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, intitule_formation VARCHAR(255) NOT NULL, objectif_formation VARCHAR(3000) NOT NULL, resultats_attendus VARCHAR(3000) NOT NULL, contenu_formation VARCHAR(3000) NOT NULL, identification_module VARCHAR(255) NOT NULL, certifiante INT NOT NULL, objectif_general_formaion INT NOT NULL, parcours_de_formation INT NOT NULL, positionnement INT NOT NULL, code_niveau_entree INT NOT NULL, code_niveau_sortie INT NOT NULL, UNIQUE INDEX UNIQ_404021BF6C27B30 (ais_id), UNIQUE INDEX UNIQ_404021BF2521174A (type_parcours_id), UNIQUE INDEX UNIQ_404021BFB572BA02 (type_positionnement_id), UNIQUE INDEX UNIQ_404021BFAAC4B70E (niveaux_id), INDEX IDX_404021BF744B06F5 (organisme_formation_responsable_id), INDEX IDX_404021BFE22A2443 (domaine_formation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geolocalisation (id INT AUTO_INCREMENT NOT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, latitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lheo (id INT AUTO_INCREMENT NOT NULL, offre_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_D3F7EE144CC8505A (offre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lieu_de_formation (id INT AUTO_INCREMENT NOT NULL, action_id INT DEFAULT NULL, coordonnees_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_1E260C669D32F035 (action_id), UNIQUE INDEX UNIQ_1E260C665853DEDF (coordonnees_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE module_prerequis (id INT AUTO_INCREMENT NOT NULL, formation_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, reference_module VARCHAR(3000) NOT NULL, UNIQUE INDEX UNIQ_9082F4C15200282E (formation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sous_module (id INT AUTO_INCREMENT NOT NULL, sous_modules_id INT DEFAULT NULL, dict_type_module_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, reference_module VARCHAR(3000) NOT NULL, type_module INT NOT NULL, INDEX IDX_DC3BD4DF86AE89A8 (sous_modules_id), UNIQUE INDEX UNIQ_DC3BD4DF7CA2D7F1 (dict_type_module_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sous_modules (id INT AUTO_INCREMENT NOT NULL, formation_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_DBE3B9A5200282E (formation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE offres (id INT AUTO_INCREMENT NOT NULL, formation_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, INDEX IDX_C6AC35445200282E (formation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_organisme (id INT AUTO_INCREMENT NOT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE coordonnees_organisme (id INT AUTO_INCREMENT NOT NULL, organisme_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_6BE8BF85DDD38F5 (organisme_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE organisme_financeur (id INT AUTO_INCREMENT NOT NULL, dict_financeurs_id INT DEFAULT NULL, action_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, nb_places_financees INT NOT NULL, code_financeurs INT NOT NULL, UNIQUE INDEX UNIQ_FA5217B9784BD9D2 (dict_financeurs_id), UNIQUE INDEX UNIQ_FA5217B99D32F035 (action_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE organisme_formateur (id INT AUTO_INCREMENT NOT NULL, action_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, siret VARCHAR(14) NOT NULL, raison_social_formateur VARCHAR(250) NOT NULL, UNIQUE INDEX UNIQ_A41FD0089D32F035 (action_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE organisme_formation_responsable (id INT AUTO_INCREMENT NOT NULL, contact_organisme_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, siret VARCHAR(14) NOT NULL, raison_social VARCHAR(250) NOT NULL, nom_organisme VARCHAR(255) NOT NULL, numero_activite VARCHAR(11) NOT NULL, renseignement_specifique VARCHAR(3000) NOT NULL, UNIQUE INDEX UNIQ_B59C780B716A0AC1 (contact_organisme_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE organisme_formation_responsable_potentiel (organisme_formation_responsable_id INT NOT NULL, potentiel_id INT NOT NULL, INDEX IDX_72CD49FA744B06F5 (organisme_formation_responsable_id), INDEX IDX_72CD49FA3E7A70EB (potentiel_id), PRIMARY KEY(organisme_formation_responsable_id, potentiel_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE periode (id INT AUTO_INCREMENT NOT NULL, session_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, debut DATE NOT NULL, fin DATE NOT NULL, UNIQUE INDEX UNIQ_93C32DF3613FECDF (session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE periode_inscription (id INT AUTO_INCREMENT NOT NULL, periode_id INT DEFAULT NULL, session_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_1EA67145F384C1CF (periode_id), UNIQUE INDEX UNIQ_1EA67145613FECDF (session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE potentiel (id INT AUTO_INCREMENT NOT NULL, organisme_formateur_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, code_formacode VARCHAR(5) NOT NULL, UNIQUE INDEX UNIQ_86A8B36464625CC6 (organisme_formateur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session (id INT AUTO_INCREMENT NOT NULL, action_id INT DEFAULT NULL, dict_etat_recrutement_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, modalites_inscritpion VARCHAR(255) NOT NULL, etat_recrutement INT NOT NULL, INDEX IDX_D044D5D49D32F035 (action_id), UNIQUE INDEX UNIQ_D044D5D4F9AB8DCA (dict_etat_recrutement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE url_action (id INT AUTO_INCREMENT NOT NULL, action_id INT DEFAULT NULL, url_web_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, INDEX IDX_8968CC629D32F035 (action_id), UNIQUE INDEX UNIQ_8968CC6230BC07BD (url_web_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE url_formation (id INT AUTO_INCREMENT NOT NULL, formation_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_D4467A555200282E (formation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE url_web (id INT AUTO_INCREMENT NOT NULL, url_formation_id INT DEFAULT NULL, web_id INT DEFAULT NULL, attributs LONGTEXT DEFAULT NULL, extras LONGTEXT DEFAULT NULL, url_web VARCHAR(400) NOT NULL, INDEX IDX_40F2E8C346595C66 (url_formation_id), INDEX IDX_40F2E8C3FE18474D (web_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE action ADD CONSTRAINT FK_47CC8C92E1BF7BE9 FOREIGN KEY (dict_perimetre_recrutement_id) REFERENCES dict_perimetre_recrutement (id)');
        $this->addSql('ALTER TABLE action ADD CONSTRAINT FK_47CC8C924D0DC4DD FOREIGN KEY (dict_modalites_es_id) REFERENCES dict_modalites_es (id)');
        $this->addSql('ALTER TABLE action ADD CONSTRAINT FK_47CC8C92F2133BF5 FOREIGN KEY (dict_modalites_enseignement_id) REFERENCES dict_modalites_enseignement (id)');
        $this->addSql('ALTER TABLE action ADD CONSTRAINT FK_47CC8C92EFAE244F FOREIGN KEY (dict_boolean_id) REFERENCES dict_boolean (id)');
        $this->addSql('ALTER TABLE action ADD CONSTRAINT FK_47CC8C925200282E FOREIGN KEY (formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE adresse ADD CONSTRAINT FK_C35F08164046BF3D FOREIGN KEY (geolocalisation_id) REFERENCES geolocalisation (id)');
        $this->addSql('ALTER TABLE adresse_information ADD CONSTRAINT FK_E777E16C9D32F035 FOREIGN KEY (action_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE adresse_information ADD CONSTRAINT FK_E777E16C4DE7DC5C FOREIGN KEY (adresse_id) REFERENCES adresse (id)');
        $this->addSql('ALTER TABLE adresse_inscription ADD CONSTRAINT FK_909E0F39613FECDF FOREIGN KEY (session_id) REFERENCES session (id)');
        $this->addSql('ALTER TABLE adresse_inscription ADD CONSTRAINT FK_909E0F394DE7DC5C FOREIGN KEY (adresse_id) REFERENCES adresse (id)');
        $this->addSql('ALTER TABLE certification ADD CONSTRAINT FK_6C3C6D755200282E FOREIGN KEY (formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE numtel ADD CONSTRAINT FK_7434FDF5E2C1A084 FOREIGN KEY (portable_id) REFERENCES portable (id)');
        $this->addSql('ALTER TABLE numtel ADD CONSTRAINT FK_7434FDF5FCB68748 FOREIGN KEY (fax_id) REFERENCES fax (id)');
        $this->addSql('ALTER TABLE numtel ADD CONSTRAINT FK_7434FDF5BF245B23 FOREIGN KEY (tel_fixe_id) REFERENCES tel_fixe (id)');
        $this->addSql('ALTER TABLE contact_formation ADD CONSTRAINT FK_2EB102C45200282E FOREIGN KEY (formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7A64625CC6 FOREIGN KEY (organisme_formateur_id) REFERENCES organisme_formateur (id)');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7A7EB71A1A FOREIGN KEY (contact_formation_id) REFERENCES contact_formation (id)');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7A716A0AC1 FOREIGN KEY (contact_organisme_id) REFERENCES coordonnees_organisme (id)');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7A4DE7DC5C FOREIGN KEY (adresse_id) REFERENCES adresse (id)');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7AFE18474D FOREIGN KEY (web_id) REFERENCES web (id)');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7ABF245B23 FOREIGN KEY (tel_fixe_id) REFERENCES tel_fixe (id)');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7AE2C1A084 FOREIGN KEY (portable_id) REFERENCES portable (id)');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7AFCB68748 FOREIGN KEY (fax_id) REFERENCES fax (id)');
        $this->addSql('ALTER TABLE date_information ADD CONSTRAINT FK_16002D5F9D32F035 FOREIGN KEY (action_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BF6C27B30 FOREIGN KEY (ais_id) REFERENCES dict_ais (id)');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BF2521174A FOREIGN KEY (type_parcours_id) REFERENCES dict_type_parcours (id)');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BFB572BA02 FOREIGN KEY (type_positionnement_id) REFERENCES dict_type_positionnement (id)');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BFAAC4B70E FOREIGN KEY (niveaux_id) REFERENCES dict_type_niveaux (id)');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BF744B06F5 FOREIGN KEY (organisme_formation_responsable_id) REFERENCES organisme_formation_responsable (id)');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BFE22A2443 FOREIGN KEY (domaine_formation_id) REFERENCES domaine_formation (id)');
        $this->addSql('ALTER TABLE lheo ADD CONSTRAINT FK_D3F7EE144CC8505A FOREIGN KEY (offre_id) REFERENCES offres (id)');
        $this->addSql('ALTER TABLE lieu_de_formation ADD CONSTRAINT FK_1E260C669D32F035 FOREIGN KEY (action_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE lieu_de_formation ADD CONSTRAINT FK_1E260C665853DEDF FOREIGN KEY (coordonnees_id) REFERENCES coordonnees (id)');
        $this->addSql('ALTER TABLE module_prerequis ADD CONSTRAINT FK_9082F4C15200282E FOREIGN KEY (formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE sous_module ADD CONSTRAINT FK_DC3BD4DF86AE89A8 FOREIGN KEY (sous_modules_id) REFERENCES sous_modules (id)');
        $this->addSql('ALTER TABLE sous_module ADD CONSTRAINT FK_DC3BD4DF7CA2D7F1 FOREIGN KEY (dict_type_module_id) REFERENCES dict_type_module (id)');
        $this->addSql('ALTER TABLE sous_modules ADD CONSTRAINT FK_DBE3B9A5200282E FOREIGN KEY (formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE offres ADD CONSTRAINT FK_C6AC35445200282E FOREIGN KEY (formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE coordonnees_organisme ADD CONSTRAINT FK_6BE8BF85DDD38F5 FOREIGN KEY (organisme_id) REFERENCES organisme_formation_responsable (id)');
        $this->addSql('ALTER TABLE organisme_financeur ADD CONSTRAINT FK_FA5217B9784BD9D2 FOREIGN KEY (dict_financeurs_id) REFERENCES dict_financeurs (id)');
        $this->addSql('ALTER TABLE organisme_financeur ADD CONSTRAINT FK_FA5217B99D32F035 FOREIGN KEY (action_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE organisme_formateur ADD CONSTRAINT FK_A41FD0089D32F035 FOREIGN KEY (action_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE organisme_formation_responsable ADD CONSTRAINT FK_B59C780B716A0AC1 FOREIGN KEY (contact_organisme_id) REFERENCES contact_organisme (id)');
        $this->addSql('ALTER TABLE organisme_formation_responsable_potentiel ADD CONSTRAINT FK_72CD49FA744B06F5 FOREIGN KEY (organisme_formation_responsable_id) REFERENCES organisme_formation_responsable (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE organisme_formation_responsable_potentiel ADD CONSTRAINT FK_72CD49FA3E7A70EB FOREIGN KEY (potentiel_id) REFERENCES potentiel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE periode ADD CONSTRAINT FK_93C32DF3613FECDF FOREIGN KEY (session_id) REFERENCES session (id)');
        $this->addSql('ALTER TABLE periode_inscription ADD CONSTRAINT FK_1EA67145F384C1CF FOREIGN KEY (periode_id) REFERENCES periode (id)');
        $this->addSql('ALTER TABLE periode_inscription ADD CONSTRAINT FK_1EA67145613FECDF FOREIGN KEY (session_id) REFERENCES session (id)');
        $this->addSql('ALTER TABLE potentiel ADD CONSTRAINT FK_86A8B36464625CC6 FOREIGN KEY (organisme_formateur_id) REFERENCES organisme_formateur (id)');
        $this->addSql('ALTER TABLE session ADD CONSTRAINT FK_D044D5D49D32F035 FOREIGN KEY (action_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE session ADD CONSTRAINT FK_D044D5D4F9AB8DCA FOREIGN KEY (dict_etat_recrutement_id) REFERENCES dict_etat_recrutement (id)');
        $this->addSql('ALTER TABLE url_action ADD CONSTRAINT FK_8968CC629D32F035 FOREIGN KEY (action_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE url_action ADD CONSTRAINT FK_8968CC6230BC07BD FOREIGN KEY (url_web_id) REFERENCES url_web (id)');
        $this->addSql('ALTER TABLE url_formation ADD CONSTRAINT FK_D4467A555200282E FOREIGN KEY (formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE url_web ADD CONSTRAINT FK_40F2E8C346595C66 FOREIGN KEY (url_formation_id) REFERENCES url_formation (id)');
        $this->addSql('ALTER TABLE url_web ADD CONSTRAINT FK_40F2E8C3FE18474D FOREIGN KEY (web_id) REFERENCES web (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE adresse_information DROP FOREIGN KEY FK_E777E16C9D32F035');
        $this->addSql('ALTER TABLE date_information DROP FOREIGN KEY FK_16002D5F9D32F035');
        $this->addSql('ALTER TABLE lieu_de_formation DROP FOREIGN KEY FK_1E260C669D32F035');
        $this->addSql('ALTER TABLE organisme_financeur DROP FOREIGN KEY FK_FA5217B99D32F035');
        $this->addSql('ALTER TABLE organisme_formateur DROP FOREIGN KEY FK_A41FD0089D32F035');
        $this->addSql('ALTER TABLE session DROP FOREIGN KEY FK_D044D5D49D32F035');
        $this->addSql('ALTER TABLE url_action DROP FOREIGN KEY FK_8968CC629D32F035');
        $this->addSql('ALTER TABLE adresse_information DROP FOREIGN KEY FK_E777E16C4DE7DC5C');
        $this->addSql('ALTER TABLE adresse_inscription DROP FOREIGN KEY FK_909E0F394DE7DC5C');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7A4DE7DC5C');
        $this->addSql('ALTER TABLE numtel DROP FOREIGN KEY FK_7434FDF5FCB68748');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7AFCB68748');
        $this->addSql('ALTER TABLE numtel DROP FOREIGN KEY FK_7434FDF5E2C1A084');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7AE2C1A084');
        $this->addSql('ALTER TABLE numtel DROP FOREIGN KEY FK_7434FDF5BF245B23');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7ABF245B23');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7AFE18474D');
        $this->addSql('ALTER TABLE url_web DROP FOREIGN KEY FK_40F2E8C3FE18474D');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7A7EB71A1A');
        $this->addSql('ALTER TABLE lieu_de_formation DROP FOREIGN KEY FK_1E260C665853DEDF');
        $this->addSql('ALTER TABLE formation DROP FOREIGN KEY FK_404021BFE22A2443');
        $this->addSql('ALTER TABLE action DROP FOREIGN KEY FK_47CC8C925200282E');
        $this->addSql('ALTER TABLE certification DROP FOREIGN KEY FK_6C3C6D755200282E');
        $this->addSql('ALTER TABLE contact_formation DROP FOREIGN KEY FK_2EB102C45200282E');
        $this->addSql('ALTER TABLE module_prerequis DROP FOREIGN KEY FK_9082F4C15200282E');
        $this->addSql('ALTER TABLE sous_modules DROP FOREIGN KEY FK_DBE3B9A5200282E');
        $this->addSql('ALTER TABLE offres DROP FOREIGN KEY FK_C6AC35445200282E');
        $this->addSql('ALTER TABLE url_formation DROP FOREIGN KEY FK_D4467A555200282E');
        $this->addSql('ALTER TABLE adresse DROP FOREIGN KEY FK_C35F08164046BF3D');
        $this->addSql('ALTER TABLE sous_module DROP FOREIGN KEY FK_DC3BD4DF86AE89A8');
        $this->addSql('ALTER TABLE lheo DROP FOREIGN KEY FK_D3F7EE144CC8505A');
        $this->addSql('ALTER TABLE organisme_formation_responsable DROP FOREIGN KEY FK_B59C780B716A0AC1');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7A716A0AC1');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7A64625CC6');
        $this->addSql('ALTER TABLE potentiel DROP FOREIGN KEY FK_86A8B36464625CC6');
        $this->addSql('ALTER TABLE formation DROP FOREIGN KEY FK_404021BF744B06F5');
        $this->addSql('ALTER TABLE coordonnees_organisme DROP FOREIGN KEY FK_6BE8BF85DDD38F5');
        $this->addSql('ALTER TABLE organisme_formation_responsable_potentiel DROP FOREIGN KEY FK_72CD49FA744B06F5');
        $this->addSql('ALTER TABLE periode_inscription DROP FOREIGN KEY FK_1EA67145F384C1CF');
        $this->addSql('ALTER TABLE organisme_formation_responsable_potentiel DROP FOREIGN KEY FK_72CD49FA3E7A70EB');
        $this->addSql('ALTER TABLE adresse_inscription DROP FOREIGN KEY FK_909E0F39613FECDF');
        $this->addSql('ALTER TABLE periode DROP FOREIGN KEY FK_93C32DF3613FECDF');
        $this->addSql('ALTER TABLE periode_inscription DROP FOREIGN KEY FK_1EA67145613FECDF');
        $this->addSql('ALTER TABLE url_web DROP FOREIGN KEY FK_40F2E8C346595C66');
        $this->addSql('ALTER TABLE url_action DROP FOREIGN KEY FK_8968CC6230BC07BD');
        $this->addSql('DROP TABLE action');
        $this->addSql('DROP TABLE adresse');
        $this->addSql('DROP TABLE adresse_information');
        $this->addSql('DROP TABLE adresse_inscription');
        $this->addSql('DROP TABLE certification');
        $this->addSql('DROP TABLE fax');
        $this->addSql('DROP TABLE numtel');
        $this->addSql('DROP TABLE portable');
        $this->addSql('DROP TABLE tel_fixe');
        $this->addSql('DROP TABLE web');
        $this->addSql('DROP TABLE contact_formation');
        $this->addSql('DROP TABLE coordonnees');
        $this->addSql('DROP TABLE date_information');
        $this->addSql('DROP TABLE domaine_formation');
        $this->addSql('DROP TABLE formation');
        $this->addSql('DROP TABLE geolocalisation');
        $this->addSql('DROP TABLE lheo');
        $this->addSql('DROP TABLE lieu_de_formation');
        $this->addSql('DROP TABLE module_prerequis');
        $this->addSql('DROP TABLE sous_module');
        $this->addSql('DROP TABLE sous_modules');
        $this->addSql('DROP TABLE offres');
        $this->addSql('DROP TABLE contact_organisme');
        $this->addSql('DROP TABLE coordonnees_organisme');
        $this->addSql('DROP TABLE organisme_financeur');
        $this->addSql('DROP TABLE organisme_formateur');
        $this->addSql('DROP TABLE organisme_formation_responsable');
        $this->addSql('DROP TABLE organisme_formation_responsable_potentiel');
        $this->addSql('DROP TABLE periode');
        $this->addSql('DROP TABLE periode_inscription');
        $this->addSql('DROP TABLE potentiel');
        $this->addSql('DROP TABLE session');
        $this->addSql('DROP TABLE url_action');
        $this->addSql('DROP TABLE url_formation');
        $this->addSql('DROP TABLE url_web');
    }
}
