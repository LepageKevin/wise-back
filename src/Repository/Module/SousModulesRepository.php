<?php

namespace App\Repository\Module;

use App\Entity\Module\SousModules;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SousModules|null find($id, $lockMode = null, $lockVersion = null)
 * @method SousModules|null findOneBy(array $criteria, array $orderBy = null)
 * @method SousModules[]    findAll()
 * @method SousModules[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SousModulesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SousModules::class);
    }

    // /**
    //  * @return SousModules[] Returns an array of SousModules objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SousModules
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
