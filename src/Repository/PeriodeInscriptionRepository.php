<?php

namespace App\Repository;

use App\Entity\PeriodeInscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PeriodeInscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method PeriodeInscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method PeriodeInscription[]    findAll()
 * @method PeriodeInscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PeriodeInscriptionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PeriodeInscription::class);
    }

    // /**
    //  * @return PeriodeInscription[] Returns an array of PeriodeInscription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PeriodeInscription
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
