<?php

namespace App\Repository\Url;

use App\Entity\Url\UrlWeb;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UrlWeb|null find($id, $lockMode = null, $lockVersion = null)
 * @method UrlWeb|null findOneBy(array $criteria, array $orderBy = null)
 * @method UrlWeb[]    findAll()
 * @method UrlWeb[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UrlWebRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UrlWeb::class);
    }

    // /**
    //  * @return UrlWeb[] Returns an array of UrlWeb objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UrlWeb
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
