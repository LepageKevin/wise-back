<?php

namespace App\Repository\Dictionnaire;

use App\Entity\App\Entity\Dictionnaire\DictEtatRecrutement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DictEtatRecrutement|null find($id, $lockMode = null, $lockVersion = null)
 * @method DictEtatRecrutement|null findOneBy(array $criteria, array $orderBy = null)
 * @method DictEtatRecrutement[]    findAll()
 * @method DictEtatRecrutement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DictEtatRecrutementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DictEtatRecrutement::class);
    }

    // /**
    //  * @return DictEtatRecrutement[] Returns an array of DictEtatRecrutement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DictEtatRecrutement
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
