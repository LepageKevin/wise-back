<?php

namespace App\Repository\Dictionnaire;

use App\Entity\App\Entity\Dictionnaire\DictModalitesEs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DictModalitesEs|null find($id, $lockMode = null, $lockVersion = null)
 * @method DictModalitesEs|null findOneBy(array $criteria, array $orderBy = null)
 * @method DictModalitesEs[]    findAll()
 * @method DictModalitesEs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DictModalitesEsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DictModalitesEs::class);
    }

    // /**
    //  * @return DictModalitesEs[] Returns an array of DictModalitesEs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DictModalitesEs
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
