<?php

namespace App\Repository\Dictionnaire;

use App\Entity\App\Entity\Dictionnaire\DictModalitesEnseignement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DictModalitesEnseignement|null find($id, $lockMode = null, $lockVersion = null)
 * @method DictModalitesEnseignement|null findOneBy(array $criteria, array $orderBy = null)
 * @method DictModalitesEnseignement[]    findAll()
 * @method DictModalitesEnseignement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DictModalitesEnseignementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DictModalitesEnseignement::class);
    }

    // /**
    //  * @return DictModalitesEnseignement[] Returns an array of DictModalitesEnseignement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DictModalitesEnseignement
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
