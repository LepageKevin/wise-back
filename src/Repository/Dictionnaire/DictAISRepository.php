<?php

namespace App\Repository\Dictionnaire;

use App\Entity\App\Entity\Dictionnaire\DictAIS;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DictAIS|null find($id, $lockMode = null, $lockVersion = null)
 * @method DictAIS|null findOneBy(array $criteria, array $orderBy = null)
 * @method DictAIS[]    findAll()
 * @method DictAIS[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DictAISRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DictAIS::class);
    }

    // /**
    //  * @return DictAIS[] Returns an array of DictAIS objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DictAIS
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
