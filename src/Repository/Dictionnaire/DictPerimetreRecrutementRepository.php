<?php

namespace App\Repository\Dictionnaire;

use App\Entity\App\Entity\Dictionnaire\DictPerimetreRecrutement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DictPerimetreRecrutement|null find($id, $lockMode = null, $lockVersion = null)
 * @method DictPerimetreRecrutement|null findOneBy(array $criteria, array $orderBy = null)
 * @method DictPerimetreRecrutement[]    findAll()
 * @method DictPerimetreRecrutement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DictPerimetreRecrutementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DictPerimetreRecrutement::class);
    }

    // /**
    //  * @return DictPerimetreRecrutement[] Returns an array of DictPerimetreRecrutement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DictPerimetreRecrutement
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
