<?php

namespace App\Repository\Dictionnaire;

use App\Entity\Dictionnaire\DictFinanceurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DictFinanceurs|null find($id, $lockMode = null, $lockVersion = null)
 * @method DictFinanceurs|null findOneBy(array $criteria, array $orderBy = null)
 * @method DictFinanceurs[]    findAll()
 * @method DictFinanceurs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DictFinanceursRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DictFinanceurs::class);
    }

    // /**
    //  * @return DictFinanceurs[] Returns an array of DictFinanceurs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DictFinanceurs
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
