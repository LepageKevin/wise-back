<?php

namespace App\Repository\Dictionnaire;

use App\Entity\App\Entity\Dictionnaire\DictTypeNiveaux;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DictTypeNiveaux|null find($id, $lockMode = null, $lockVersion = null)
 * @method DictTypeNiveaux|null findOneBy(array $criteria, array $orderBy = null)
 * @method DictTypeNiveaux[]    findAll()
 * @method DictTypeNiveaux[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DictTypeNiveauxRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DictTypeNiveaux::class);
    }

    // /**
    //  * @return DictTypeNiveaux[] Returns an array of DictTypeNiveaux objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DictTypeNiveaux
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
