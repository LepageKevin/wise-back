<?php

namespace App\Repository\Dictionnaire;

use App\Entity\App\Entity\Dictionnaire\DictBoolean;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DictBoolean|null find($id, $lockMode = null, $lockVersion = null)
 * @method DictBoolean|null findOneBy(array $criteria, array $orderBy = null)
 * @method DictBoolean[]    findAll()
 * @method DictBoolean[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DictBooleanRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DictBoolean::class);
    }

    // /**
    //  * @return DictBoolean[] Returns an array of DictBoolean objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DictBoolean
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
