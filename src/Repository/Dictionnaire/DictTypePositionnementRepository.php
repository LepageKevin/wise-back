<?php

namespace App\Repository\Dictionnaire;

use App\Entity\Dictionnaire\DictTypePositionnement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DictTypePositionnement|null find($id, $lockMode = null, $lockVersion = null)
 * @method DictTypePositionnement|null findOneBy(array $criteria, array $orderBy = null)
 * @method DictTypePositionnement[]    findAll()
 * @method DictTypePositionnement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DictTypePositionnementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DictTypePositionnement::class);
    }

    // /**
    //  * @return DictTypePositionnement[] Returns an array of DictTypePositionnement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DictTypePositionnement
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
