<?php

namespace App\Repository\Dictionnaire;

use App\Entity\Dictionnaire\DictTypeParcours;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DictTypeParcours|null find($id, $lockMode = null, $lockVersion = null)
 * @method DictTypeParcours|null findOneBy(array $criteria, array $orderBy = null)
 * @method DictTypeParcours[]    findAll()
 * @method DictTypeParcours[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DictTypeParcoursRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DictTypeParcours::class);
    }

    // /**
    //  * @return DictTypeParcours[] Returns an array of DictTypeParcours objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DictTypeParcours
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
