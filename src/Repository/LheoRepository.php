<?php

namespace App\Repository;

use App\Entity\Lheo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Lheo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lheo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lheo[]    findAll()
 * @method Lheo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LheoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Lheo::class);
    }

    // /**
    //  * @return Lheo[] Returns an array of Lheo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lheo
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
