<?php

namespace App\Repository\Contact;

use App\Entity\Contact\Numtel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Numtel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Numtel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Numtel[]    findAll()
 * @method Numtel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NumtelRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Numtel::class);
    }

    // /**
    //  * @return Numtel[] Returns an array of Numtel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Numtel
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
