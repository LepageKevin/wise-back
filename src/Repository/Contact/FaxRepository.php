<?php

namespace App\Repository\Contact;

use App\Entity\Contact\Fax;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Fax|null find($id, $lockMode = null, $lockVersion = null)
 * @method Fax|null findOneBy(array $criteria, array $orderBy = null)
 * @method Fax[]    findAll()
 * @method Fax[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FaxRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Fax::class);
    }

    // /**
    //  * @return Fax[] Returns an array of Fax objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Fax
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
