<?php

namespace App\Repository\Adresse;

use App\Entity\Adresse\AdresseInformation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AdresseInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdresseInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdresseInformation[]    findAll()
 * @method AdresseInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdresseInformationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AdresseInformation::class);
    }

    // /**
    //  * @return AdresseInformation[] Returns an array of AdresseInformation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdresseInformation
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
