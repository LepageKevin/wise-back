<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Ecouteur dans les évenements JWT pour ajout de données
 * 
 * @final
 */
final class JWTCreatedListener
{
    /**
     * Evenement lors de la Création du Token JWT
     * Remplacer les données pour le Token JWT
     *
     * @param JWTCreatedEvent $event
     * 
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $user = $event->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }

        // ajout des nouvelles données
        $payload = \array_merge(
            $event->getData(), [
                'id' => $user->getId(),
                'email' => $user->getEmail(),
                'createdAt' => $user->getCreatedAt()->format('d-m-Y')
            ]
        );

        $event->setData($payload);
    }
}