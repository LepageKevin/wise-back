<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Gère les Routes Utilisateurs
 *
 * @Route("/api/utilisateur", name="api_user_")
 *
 * @final
 */
final class AuthController
{
    /**
     * Manageur d'Entité
     *
     * @var ObjectManager
     */
    private $em;

    /**
     * Encodeur de Mot de Passe
     *
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * Validateur
     *
     * @var ValidatorInterface
     */
    private $validator;


    /**
     * Constructeur
     *
     * @param ObjectManager                $em        Une implémentation de ObjectManager (injectée)
     * @param UserPasswordEncoderInterface $encoder   Une implémentation de UserPasswordEncoderInterface (injectée)
     * @param ValidatorInterface           $validator Une implémentation de ValidatorInterface (injectée)
     */
    public function __construct(
        ObjectManager $em,
        UserPasswordEncoderInterface $encoder,
        ValidatorInterface $validator
    )
    {
        $this->em = $em;
        $this->encoder = $encoder;
        $this->validator = $validator;
    }

    /**
     * Route pour Inscrire un nouvel Utilisateur
     *
     * @Route("/inscription", name="register", methods={"POST", "OPTIONS"})
     *
     * @param Request $request La Requête Courante
     *
     * @return Response
     */
    public function registerRoute(Request $request): Response
    {
        $values = \json_decode($request->getContent(), true);
        $username = $values['username'];
        $email = $values['email'];
        $password = $values['password'];

        if (!$username || !$email || !$password) {
            return new JsonResponse(["error" => "Tous les champs sont requis !"]);
        }

        $user = new User();
        $user->setUsername($username)
            ->setEmail($email)
            ->setPlainPassword($password);

        $errors = $this->validator->validate($user);

        // validation des données entrées (longueur mot de passe, etc...)
        $errors = $this->getValidatorMessages($user);
        if ($errors !== null) {
            return new JsonResponse(["error" => $errors]);
        }

        // aucune violations dans les données.
        // encodage du Mot de Passe avant de pousser dans la BDD
        $user->setPassword(
            $this->encoder->encodePassword($user, $user->getPlainPassword())
        );

        $this->em->persist($user);
        $this->em->flush();

        return new JsonResponse(["success" => "L'utilisateur a été crée"]);
    }


    /**
     * Route pour modifier un Utilisateur (Token requis)
     * 
     * @Route("/modifier", name="update", methods={"POST", "OPTIONS"})
     * 
     * @param Request $request La Requête courrante
     *
     * @return Response
     */
    public function updateRoute(Request $request): Response
    {
        $values = \json_decode($request->getContent(), true);

        $id = (isset($values['id'])) ? $values['id'] : null;
        $username = (isset($values['username'])) ? $values['username'] : null;
        $email = (isset($values['email'])) ? $values['email'] : null;
        $password = (isset($values['password'])) ? $values['password'] : null;

        if (!$id || !$username || !$email) {
            return new JsonResponse(["error" => "Tous les champs sont requis !"]);
        }

        $user = $this->em->getRepository(User::class)->find($id);

        if (!$user) {
            return new JsonResponse(["error" => "Utilisateur introuvable"]);
        }

        if ($username != $user->getUsername()) {
            $user->setUsername($username);
        }

        if ($email != $user->getEmail()) {
            $user->setEmail($email);
        }

        if ($password) {
            $user->setPassword(
                $this->encoder->encodePassword($user, $password)
            );
        }

        $errors = $this->getValidatorMessages($user);
        if ($errors !== null) {
            return new JsonResponse(["error" => $errors]);
        }

        $this->em->persist($user);
        $this->em->flush();

        return new JsonResponse(["success" => "Utilisateur mis à jour"]);
    }


    /**
     * Récupère les messages du Validator après validation
     *
     * @param object $entity
     *
     * @return array|null
     */
    private function getValidatorMessages($entity)
    {
        $errors = $this->validator->validate($entity);

        if (count($errors) > 0) {
            $messages = [];

            foreach ($errors as $violation) {
                $messages[$violation->getPropertyPath()][] = $violation->getMessage();
            }

            return $messages;
        }

        return null;
    }
}
