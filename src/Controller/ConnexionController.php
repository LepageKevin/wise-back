<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ConnexionController extends AbstractController
{
    /**
     * @Route("/api/login_check", name="connexion_option", methods={"OPTIONS"})
     */
    public function index()
    {
        return new Response();
    }
}
