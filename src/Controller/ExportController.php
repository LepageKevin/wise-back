<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * S'occupe de l'export
 */
class ExportController
{
    /**
     * Manageur des Entitées
     *
     * @var ObjectManager
     */
    private $manager;


    /**
     * ExportController Constructeur
     *
     * @param ObjectManager $manager Une implémentation de ObjectManager (injectée)
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;   
    }


    /**
     * @Route("/export", name="export_route")
     *
     * @param Request $requete
     * 
     * @return Response
     */
    public function exportRoute(Request $requete): Response
    {
        return new Response("test");
    }
}