<?php

namespace App\Entity;

use App\Entity\Organisme\OrganismeFormateur;
use App\Entity\Organisme\OrganismeFormationResponsable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PotentielRepository")
 */
class Potentiel extends BasePropriete
{
    /**
     * @ORM\Column(type="string", length=5)
     */
    private $codeFormacode;
    
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Organisme\OrganismeFormateur", inversedBy="potentiel")
     */
    private $organismeFormateur;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Organisme\OrganismeFormationResponsable", mappedBy="potentiels")
     * @ORM\JoinTable(name="potentiels_organismeFormationResponsables")
     */
    private $organismeFormationResponsables;

    public function __construct()
    {
        $this->organismeFormationResponsables = new ArrayCollection();
    }

    public function getCodeFormacode(): ?string
    {
        return $this->codeFormacode;
    }

    public function setCodeFormacode(string $codeFormacode): self
    {
        $this->codeFormacode = $codeFormacode;

        return $this;
    }

    public function getOrganismeFormateur(): ?OrganismeFormateur
    {
        return $this->organismeFormateur;
    }

    public function setOrganismeFormateur(?OrganismeFormateur $organismeFormateur): self
    {
        $this->organismeFormateur = $organismeFormateur;

        return $this;
    }

    /**
     * @return Collection|OrganismeFormationResponsable[]
     */
    public function getOrganismeFormationResponsables(): Collection
    {
        return $this->organismeFormationResponsables;
    }

    public function addOrganismeFormationResponsable(OrganismeFormationResponsable $organismeFormationResponsable): self
    {
        if (!$this->organismeFormationResponsables->contains($organismeFormationResponsable)) {
            $this->organismeFormationResponsables[] = $organismeFormationResponsable;
            $organismeFormationResponsable->addPotentiel($this);
        }

        return $this;
    }

    public function removeOrganismeFormationResponsable(OrganismeFormationResponsable $organismeFormationResponsable): self
    {
        if ($this->organismeFormationResponsables->contains($organismeFormationResponsable)) {
            $this->organismeFormationResponsables->removeElement($organismeFormationResponsable);
            $organismeFormationResponsable->removePotentiel($this);
        }

        return $this;
    }
}
