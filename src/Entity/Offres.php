<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\OffresRepository")
 */
class Offres extends BasePropriete
{
    /**
     * Une Offre est liée à une Entitée de Lhéo
     * 
     * @ORM\OneToOne(targetEntity="App\Entity\Lheo", mappedBy="offre")
     *
     * @var int
     */
    private $lheo;

    /**
     * Une Offre est liée à une Formation
     * 
     * @Groups({"read", "write"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation", inversedBy="offres")
     * 
     * @var int
     */
    private $formation;

    
    public function getLheo(): ?Lheo
    {
        return $this->lheo;
    }

    public function setLheo(?Lheo $lheo): self
    {
        $this->lheo = $lheo;

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }
}
