<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classe de Base des Entitées
 *
 * @ORM\MappedSuperclass
 *
 * @abstract
 */
abstract class BasePropriete
{
    /**
     * ID de l'entitée
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $attributs;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $extras;


    /**
     * Récupère l'ID de l'entitée
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }


    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }


    public function getAttributs(): ?string
    {
        return $this->attributs;
    }

    public function setAttributs(string $attributs): self
    {
        $this->attributs = $attributs;

        return $this;
    }

    public function getExtras(): ?string
    {
        return $this->extras;
    }

    public function setExtras(string $extras): self
    {
        $this->extras = $extras;

        return $this;
    }
}