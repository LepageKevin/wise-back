<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Adresse\AdresseInformation;
use App\Entity\Dictionnaire\DictBoolean;
use App\Entity\Dictionnaire\DictModalitesEnseignement;
use App\Entity\Dictionnaire\DictModalitesEs;
use App\Entity\Dictionnaire\DictPerimetreRecrutement;
use App\Entity\Organisme\OrganismeFinanceur;
use App\Entity\Organisme\OrganismeFormateur;
use App\Entity\Url\UrlAction;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ActionRepository")
 */
class Action extends BasePropriete
{

    /**
     * @ORM\Column(type="string", length=300)
     */
    private $rythmeFormation;

    /**
     * @ORM\Column(type="string", length=3000)
     */
    private $modalitesAlternance;

    /**
     * @ORM\Column(type="string", length=3000)
     */
    private $conditionsSpecifiques;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $infoPublicVise;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $dureeIndicative;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $codeModalitePedagogique;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $codePublicVise;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $modalitePedagogique;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $fraisRestants;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $langueFormation;

    /**
     * @ORM\Column(type="string", length=3000, nullable=true)
     */
    private $modaliteRecrutement;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $infosPerimetreRecrutement;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $prixHoraireTTC;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $prixTotalTTC;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $nombreHeuresCentre;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $nombreHeuresEntreprise;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $nombreHeuresTotal;

    /**
     * @ORM\Column(type="string", length=600, nullable=true)
     */
    private $detailConditionPriseEnCharge;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $dureeConvention;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $restauration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hebergement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $transport;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $accesHandicapes;

    /**
     * @ORM\Column(type="integer")
     */
    private $niveauEntreeObligatoire;

    /**
     * @ORM\Column(type="integer")
     */
    private $priseEnChargeFraisPossible;

    /**
     * @ORM\Column(type="integer")
     */
    private $conventionnement;

    /**
     * @ORM\Column(type="integer")
     */
    private $modalitesEnseignement;

    /**
     * @ORM\Column(type="integer")
     */
    private $modalitesEntreeSorties;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Adresse\AdresseInformation", mappedBy="action")
     */
    private $adresseInformation;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\LieuDeFormation", mappedBy="action")
     */
    private $lieuDeFormation;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Organisme\OrganismeFormateur", mappedBy="action")
     */
    private $organismeFormateur;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\DateInformation", mappedBy="action")
     */
    private $dateInformation;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Url\UrlAction", mappedBy="action")
     */
    private $urlsAction;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Organisme\OrganismeFinanceur", mappedBy="action")
     */
    private $organismeFinanceur;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dictionnaire\DictPerimetreRecrutement")
     */
    private $dictPerimetreRecrutement;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dictionnaire\DictModalitesEs")
     */
    private $dictModalitesES;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dictionnaire\DictModalitesEnseignement")
     */
    private $dictModalitesEnseignement;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dictionnaire\DictBoolean")
     */
    private $dictBoolean;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation", inversedBy="actions")
     */
    private $formation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Session", mappedBy="action")
     */
    private $sessions;
    

    public function __construct()
    {
        $this->urlsAction = new ArrayCollection();
        $this->sessions = new ArrayCollection();
    }

    public function getRythmeFormation(): ?string
    {
        return $this->rythmeFormation;
    }

    public function setRythmeFormation(string $rythmeFormation): self
    {
        $this->rythmeFormation = $rythmeFormation;

        return $this;
    }

    public function getModalitesAlternance(): ?string
    {
        return $this->modalitesAlternance;
    }

    public function setModalitesAlternance(string $modalitesAlternance): self
    {
        $this->modalitesAlternance = $modalitesAlternance;

        return $this;
    }

    public function getConditionsSpecifiques(): ?string
    {
        return $this->conditionsSpecifiques;
    }

    public function setConditionsSpecifiques(string $conditionsSpecifiques): self
    {
        $this->conditionsSpecifiques = $conditionsSpecifiques;

        return $this;
    }

    public function getInfoPublicVise(): ?string
    {
        return $this->infoPublicVise;
    }

    public function setInfoPublicVise(string $infoPublicVise): self
    {
        $this->infoPublicVise = $infoPublicVise;

        return $this;
    }

    public function getDureeIndicative(): ?string
    {
        return $this->dureeIndicative;
    }

    public function setDureeIndicative(string $dureeIndicative): self
    {
        $this->dureeIndicative = $dureeIndicative;

        return $this;
    }

    public function getCodeModalitePedagogique(): ?string
    {
        return $this->codeModalitePedagogique;
    }

    public function setCodeModalitePedagogique(?string $codeModalitePedagogique): self
    {
        $this->codeModalitePedagogique = $codeModalitePedagogique;

        return $this;
    }

    public function getFraisRestants(): ?string
    {
        return $this->fraisRestants;
    }

    public function setFraisRestants(?string $fraisRestants): self
    {
        $this->fraisRestants = $fraisRestants;

        return $this;
    }

    public function getLangueFormation(): ?string
    {
        return $this->langueFormation;
    }

    public function setLangueFormation(?string $langueFormation): self
    {
        $this->langueFormation = $langueFormation;

        return $this;
    }

    public function getModaliteRecrutement(): ?string
    {
        return $this->modaliteRecrutement;
    }

    public function setModaliteRecrutement(?string $modaliteRecrutement): self
    {
        $this->modaliteRecrutement = $modaliteRecrutement;

        return $this;
    }

    public function getInfosPerimetreRecrutement(): ?string
    {
        return $this->infosPerimetreRecrutement;
    }

    public function setInfosPerimetreRecrutement(?string $infosPerimetreRecrutement): self
    {
        $this->infosPerimetreRecrutement = $infosPerimetreRecrutement;

        return $this;
    }

    public function getPrixHoraireTTC(): ?string
    {
        return $this->prixHoraireTTC;
    }

    public function setPrixHoraireTTC(?string $prixHoraireTTC): self
    {
        $this->prixHoraireTTC = $prixHoraireTTC;

        return $this;
    }

    public function getPrixTotalTTC(): ?string
    {
        return $this->prixTotalTTC;
    }

    public function setPrixTotalTTC(string $prixTotalTTC): self
    {
        $this->prixTotalTTC = $prixTotalTTC;

        return $this;
    }

    public function getNombreHeuresCentre(): ?float
    {
        return $this->nombreHeuresCentre;
    }

    public function setNombreHeuresCentre(?float $nombreHeuresCentre): self
    {
        $this->nombreHeuresCentre = $nombreHeuresCentre;

        return $this;
    }

    public function getNombreHeuresEntreprise(): ?float
    {
        return $this->nombreHeuresEntreprise;
    }

    public function setNombreHeuresEntreprise(?float $nombreHeuresEntreprise): self
    {
        $this->nombreHeuresEntreprise = $nombreHeuresEntreprise;

        return $this;
    }

    public function getNombreHeuresTotal(): ?float
    {
        return $this->nombreHeuresTotal;
    }

    public function setNombreHeuresTotal(?float $nombreHeuresTotal): self
    {
        $this->nombreHeuresTotal = $nombreHeuresTotal;

        return $this;
    }

    public function getDetailConditionPriseEnCharge(): ?string
    {
        return $this->detailConditionPriseEnCharge;
    }

    public function setDetailConditionPriseEnCharge(?string $detailConditionPriseEnCharge): self
    {
        $this->detailConditionPriseEnCharge = $detailConditionPriseEnCharge;

        return $this;
    }

    public function getDureeConvention(): ?float
    {
        return $this->dureeConvention;
    }

    public function setDureeConvention(?float $dureeConvention): self
    {
        $this->dureeConvention = $dureeConvention;

        return $this;
    }

    public function getRestauration(): ?string
    {
        return $this->restauration;
    }

    public function setRestauration(?string $restauration): self
    {
        $this->restauration = $restauration;

        return $this;
    }

    public function getHebergement(): ?string
    {
        return $this->hebergement;
    }

    public function setHebergement(?string $hebergement): self
    {
        $this->hebergement = $hebergement;

        return $this;
    }

    public function getTransport(): ?string
    {
        return $this->transport;
    }

    public function setTransport(?string $transport): self
    {
        $this->transport = $transport;

        return $this;
    }

    public function getAccesHandicapes(): ?string
    {
        return $this->accesHandicapes;
    }

    public function setAccesHandicapes(string $accesHandicapes): self
    {
        $this->accesHandicapes = $accesHandicapes;

        return $this;
    }

    public function getNiveauEntreeObligatoire(): ?int
    {
        return $this->niveauEntreeObligatoire;
    }

    public function setNiveauEntreeObligatoire(int $niveauEntreeObligatoire): self
    {
        $this->niveauEntreeObligatoire = $niveauEntreeObligatoire;

        return $this;
    }

    public function getPriseEnChargeFraisPossible(): ?int
    {
        return $this->priseEnChargeFraisPossible;
    }

    public function setPriseEnChargeFraisPossible(int $priseEnChargeFraisPossible): self
    {
        $this->priseEnChargeFraisPossible = $priseEnChargeFraisPossible;

        return $this;
    }

    public function getConventionnement(): ?int
    {
        return $this->conventionnement;
    }

    public function setConventionnement(int $conventionnement): self
    {
        $this->conventionnement = $conventionnement;

        return $this;
    }

    public function getModalitesEnseignement(): ?int
    {
        return $this->modalitesEnseignement;
    }

    public function setModalitesEnseignement(int $modalitesEnseignement): self
    {
        $this->modalitesEnseignement = $modalitesEnseignement;

        return $this;
    }

    public function getModalitesEntreeSorties(): ?int
    {
        return $this->modalitesEntreeSorties;
    }

    public function setModalitesEntreeSorties(int $modalitesEntreeSorties): self
    {
        $this->modalitesEntreeSorties = $modalitesEntreeSorties;

        return $this;
    }

    public function getAdresseInformation(): ?AdresseInformation
    {
        return $this->adresseInformation;
    }

    public function setAdresseInformation(?AdresseInformation $adresseInformation): self
    {
        $this->adresseInformation = $adresseInformation;

        // set (or unset) the owning side of the relation if necessary
        $newAction = $adresseInformation === null ? null : $this;
        if ($newAction !== $adresseInformation->getAction()) {
            $adresseInformation->setAction($newAction);
        }

        return $this;
    }

    public function getLieuDeFormation(): ?LieuDeFormation
    {
        return $this->lieuDeFormation;
    }

    public function setLieuDeFormation(?LieuDeFormation $lieuDeFormation): self
    {
        $this->lieuDeFormation = $lieuDeFormation;

        // set (or unset) the owning side of the relation if necessary
        $newAction = $lieuDeFormation === null ? null : $this;
        if ($newAction !== $lieuDeFormation->getAction()) {
            $lieuDeFormation->setAction($newAction);
        }

        return $this;
    }

    public function getOrganismeFormateur(): ?OrganismeFormateur
    {
        return $this->organismeFormateur;
    }

    public function setOrganismeFormateur(?OrganismeFormateur $organismeFormateur): self
    {
        $this->organismeFormateur = $organismeFormateur;

        // set (or unset) the owning side of the relation if necessary
        $newAction = $organismeFormateur === null ? null : $this;
        if ($newAction !== $organismeFormateur->getAction()) {
            $organismeFormateur->setAction($newAction);
        }

        return $this;
    }

    public function getDateInformation(): ?DateInformation
    {
        return $this->dateInformation;
    }

    public function setDateInformation(?DateInformation $dateInformation): self
    {
        $this->dateInformation = $dateInformation;

        // set (or unset) the owning side of the relation if necessary
        $newAction = $dateInformation === null ? null : $this;
        if ($newAction !== $dateInformation->getAction()) {
            $dateInformation->setAction($newAction);
        }

        return $this;
    }

    /**
     * @return Collection|UrlAction[]
     */
    public function getUrlsAction(): Collection
    {
        return $this->urlsAction;
    }

    public function addUrlsAction(UrlAction $urlsAction): self
    {
        if (!$this->urlsAction->contains($urlsAction)) {
            $this->urlsAction[] = $urlsAction;
            $urlsAction->setAction($this);
        }

        return $this;
    }

    public function removeUrlsAction(UrlAction $urlsAction): self
    {
        if ($this->urlsAction->contains($urlsAction)) {
            $this->urlsAction->removeElement($urlsAction);
            // set the owning side to null (unless already changed)
            if ($urlsAction->getAction() === $this) {
                $urlsAction->setAction(null);
            }
        }

        return $this;
    }

    public function getOrganismeFinanceur(): ?OrganismeFinanceur
    {
        return $this->organismeFinanceur;
    }

    public function setOrganismeFinanceur(?OrganismeFinanceur $organismeFinanceur): self
    {
        $this->organismeFinanceur = $organismeFinanceur;

        // set (or unset) the owning side of the relation if necessary
        $newAction = $organismeFinanceur === null ? null : $this;
        if ($newAction !== $organismeFinanceur->getAction()) {
            $organismeFinanceur->setAction($newAction);
        }

        return $this;
    }

    public function getDictPerimetreRecrutement(): ?DictPerimetreRecrutement
    {
        return $this->dictPerimetreRecrutement;
    }

    public function setDictPerimetreRecrutement(?DictPerimetreRecrutement $dictPerimetreRecrutement): self
    {
        $this->dictPerimetreRecrutement = $dictPerimetreRecrutement;

        // set (or unset) the owning side of the relation if necessary
        $newAction = $dictPerimetreRecrutement === null ? null : $this;
        if ($newAction !== $dictPerimetreRecrutement->getAction()) {
            $dictPerimetreRecrutement->setAction($newAction);
        }

        return $this;
    }

    public function getDictModalitesES(): ?DictModalitesEs
    {
        return $this->dictModalitesES;
    }

    public function setDictModalitesES(?DictModalitesEs $dictModalitesES): self
    {
        $this->dictModalitesES = $dictModalitesES;

        // set (or unset) the owning side of the relation if necessary
        $newAction = $dictModalitesES === null ? null : $this;
        if ($newAction !== $dictModalitesES->getAction()) {
            $dictModalitesES->setAction($newAction);
        }

        return $this;
    }

    public function getDictModalitesEnseignement(): ?DictModalitesEnseignement
    {
        return $this->dictModalitesEnseignement;
    }

    public function setDictModalitesEnseignement(?DictModalitesEnseignement $dictModalitesEnseignement): self
    {
        $this->dictModalitesEnseignement = $dictModalitesEnseignement;

        // set (or unset) the owning side of the relation if necessary
        $newAction = $dictModalitesEnseignement === null ? null : $this;
        if ($newAction !== $dictModalitesEnseignement->getAction()) {
            $dictModalitesEnseignement->setAction($newAction);
        }

        return $this;
    }

    public function getDictBoolean(): ?DictBoolean
    {
        return $this->dictBoolean;
    }

    public function setDictBoolean(?DictBoolean $dictBoolean): self
    {
        $this->dictBoolean = $dictBoolean;

        // set (or unset) the owning side of the relation if necessary
        $newAction = $dictBoolean === null ? null : $this;
        if ($newAction !== $dictBoolean->getAction()) {
            $dictBoolean->setAction($newAction);
        }

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->setAction($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->contains($session)) {
            $this->sessions->removeElement($session);
            // set the owning side to null (unless already changed)
            if ($session->getAction() === $this) {
                $session->setAction(null);
            }
        }

        return $this;
    }

    public function getCodePublicVise(): ?string
    {
        return $this->codePublicVise;
    }

    public function setCodePublicVise(string $codePublicVise): self
    {
        $this->codePublicVise = $codePublicVise;

        return $this;
    }

    public function getModalitePedagogique(): ?string
    {
        return $this->modalitePedagogique;
    }

    public function setModalitePedagogique(?string $modalitePedagogique): self
    {
        $this->modalitePedagogique = $modalitePedagogique;

        return $this;
    }

}
