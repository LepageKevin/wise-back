<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LieuDeFormationRepository")
 */
class LieuDeFormation extends BasePropriete
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Action", inversedBy="lieuDeFormation")
     */
    private $action;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Coordonnees", inversedBy="lieuFormation")
     */
    private $coordonnees;
    

    public function getAction(): ?Action
    {
        return $this->action;
    }

    public function setAction(?Action $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getCoordonnees(): ?Coordonnees
    {
        return $this->coordonnees;
    }

    public function setCoordonnees(?Coordonnees $coordonnees): self
    {
        $this->coordonnees = $coordonnees;

        // set (or unset) the owning side of the relation if necessary
        $newLieuFormation = $coordonnees === null ? null : $this;
        if ($newLieuFormation !== $coordonnees->getLieuFormation()) {
            $coordonnees->setLieuFormation($newLieuFormation);
        }

        return $this;
    }
}
