<?php

namespace App\Entity;

use App\Entity\Adresse\Adresse;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GeolocalisationRepository")
 */
class Geolocalisation extends BasePropriete
{
    /**
     * @ORM\Column(type="float")
     */
    private $latitude;

    /**
     * @ORM\Column(type="float")
     */
    private $longitude;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Adresse\Adresse", mappedBy="geolocalisation")
     */
    private $adresse;

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getAdresse(): ?Adresse
    {
        return $this->adresse;
    }

    public function setAdresse(?Adresse $adresse): self
    {
        $this->adresse = $adresse;

        // set (or unset) the owning side of the relation if necessary
        $newGeolocalisation = $adresse === null ? null : $this;
        if ($newGeolocalisation !== $adresse->getGeolocalisation()) {
            $adresse->setGeolocalisation($newGeolocalisation);
        }

        return $this;
    }
}
