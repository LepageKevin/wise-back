<?php

namespace App\Entity\Adresse;

use App\Entity\Session;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Adresse\AdresseInscriptionRepository")
 */
class AdresseInscription extends BasePropriete
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Session", inversedBy="adresseInscription")
     */
    private $session;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Adresse\Adresse", inversedBy="adresseInscription")
     */
    private $adresse;


    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        // set (or unset) the owning side of the relation if necessary
        $newAdresseInscription = $session === null ? null : $this;
        if ($newAdresseInscription !== $session->getAdresseInscription()) {
            $session->setAdresseInscription($newAdresseInscription);
        }

        return $this;
    }

    public function getAdresse(): ?Adresse
    {
        return $this->adresse;
    }

    public function setAdresse(?Adresse $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }
}
