<?php

namespace App\Entity\Adresse;

use App\Entity\Action;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Adresse\AdresseInformationRepository")
 */
class AdresseInformation extends BasePropriete
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Action", inversedBy="adresseInformation")
     */
    private $action;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Adresse\Adresse", inversedBy="information")
     */
    private $adresse;

    
    public function getAction(): ?Action
    {
        return $this->action;
    }

    public function setAction(?Action $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getAdresse(): ?Adresse
    {
        return $this->adresse;
    }

    public function setAdresse(?Adresse $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }
}
