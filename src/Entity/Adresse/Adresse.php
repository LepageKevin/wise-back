<?php

namespace App\Entity\Adresse;

use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\Action;
use App\Entity\Coordonnees;
use App\Entity\Geolocalisation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Adresse\AdresseRepository")
 */
class Adresse extends BasePropriete
{
    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=50)
     */
    private $ligne;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=5)
     */
    private $codePostal;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=50)
     */
    private $ville;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=3)
     */
    private $departement;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=5)
     */
    private $codeInseeCommune;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=5)
     */
    private $codeInseeCanton;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=2)
     */
    private $region;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=2)
     */
    private $pays;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Action", mappedBy="formation")
     */
    private $actions;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Coordonnees", mappedBy="adresse")
     */
    private $coordonnees;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Geolocalisation", inversedBy="adresse")
     */
    private $geolocalisation;

    /**
     * Une Adresse possède d'autres informations
     * 
     * @ORM\OneToOne(targetEntity="App\Entity\Adresse\AdresseInformation", mappedBy="adresse")
     */
    private $information;

    /**
     * Une Adresse peut être liée à une Adresse d'Inscription
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Adresse\AdresseInscription", mappedBy="adresse")
     */
    private $adresseInscription;


    public function __construct()
    {
        $this->actions = new ArrayCollection();
    }

    public function getLigne(): ?string
    {
        return $this->ligne;
    }

    public function setLigne(string $ligne): self
    {
        $this->ligne = $ligne;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    public function setDepartement(string $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getCodeInseeCommune(): ?string
    {
        return $this->codeInseeCommune;
    }

    public function setCodeInseeCommune(string $codeInseeCommune): self
    {
        $this->codeInseeCommune = $codeInseeCommune;

        return $this;
    }

    public function getCodeInseeCanton(): ?string
    {
        return $this->codeInseeCanton;
    }

    public function setCodeInseeCanton(string $codeInseeCanton): self
    {
        $this->codeInseeCanton = $codeInseeCanton;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * @return Collection|Action[]
     */
    public function getActions(): Collection
    {
        return $this->actions;
    }

    public function addAction(Action $action): self
    {
        if (!$this->actions->contains($action)) {
            $this->actions[] = $action;
            $action->setFormation($this);
        }

        return $this;
    }

    public function removeAction(Action $action): self
    {
        if ($this->actions->contains($action)) {
            $this->actions->removeElement($action);
            // set the owning side to null (unless already changed)
            if ($action->getFormation() === $this) {
                $action->setFormation(null);
            }
        }

        return $this;
    }

    public function getCoordonnees(): ?Coordonnees
    {
        return $this->coordonnees;
    }

    public function setCoordonnees(?Coordonnees $coordonnees): self
    {
        $this->coordonnees = $coordonnees;

        // set (or unset) the owning side of the relation if necessary
        $newAdresse = $coordonnees === null ? null : $this;
        if ($newAdresse !== $coordonnees->getAdresse()) {
            $coordonnees->setAdresse($newAdresse);
        }

        return $this;
    }

    public function getGeolocalisation(): ?Geolocalisation
    {
        return $this->geolocalisation;
    }

    public function setGeolocalisation(?Geolocalisation $geolocalisation): self
    {
        $this->geolocalisation = $geolocalisation;

        return $this;
    }

    public function getInformation(): ?AdresseInformation
    {
        return $this->information;
    }

    public function setInformation(?AdresseInformation $information): self
    {
        $this->information = $information;

        // set (or unset) the owning side of the relation if necessary
        $newAdresse = $information === null ? null : $this;
        if ($newAdresse !== $information->getAdresse()) {
            $information->setAdresse($newAdresse);
        }

        return $this;
    }

    public function getAdresseInscription(): ?AdresseInscription
    {
        return $this->adresseInscription;
    }

    public function setAdresseInscription(?AdresseInscription $adresseInscription): self
    {
        $this->adresseInscription = $adresseInscription;

        // set (or unset) the owning side of the relation if necessary
        $newAdresse = $adresseInscription === null ? null : $this;
        if ($newAdresse !== $adresseInscription->getAdresse()) {
            $adresseInscription->setAdresse($newAdresse);
        }

        return $this;
    }
}
