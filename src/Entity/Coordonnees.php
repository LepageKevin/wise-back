<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\Adresse\Adresse;
use App\Entity\Contact\Fax;
use App\Entity\Contact\Portable;
use App\Entity\Contact\TelFixe;
use App\Entity\Contact\Web;
use App\Entity\Organisme\CoordonneesOrganisme;
use App\Entity\Organisme\OrganismeFormateur;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CoordonneesRepository")
 */
class Coordonnees extends BasePropriete
{
    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=50)
     */
    private $civilite;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=50)
     */
    private $prenom;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=50)
     */
    private $ligne;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=160)
     */
    private $couriel;

    /**
     * @Groups({"read", "write"})
     * @ORM\OneToOne(targetEntity="App\Entity\Organisme\OrganismeFormateur", inversedBy="coordonnees")
     */
    private $organismeFormateur;

    /**
     * Des Coordonnées peuvent être des Contacts de Formation
     * 
     * @ORM\OneToOne(targetEntity="App\Entity\ContactFormation", inversedBy="coordonnees")
     *
     * @var int
     */
    private $contactFormation;

    /**
     * Des Coordonnées peuvent être des Contacts d'Organisme
     * 
     * @ORM\OneToOne(targetEntity="App\Entity\Organisme\CoordonneesOrganisme", inversedBy="coordonnees")
     *
     * @var int
     */
    private $contactOrganisme;

    /**
     * @Groups({"read", "write"})
     * @ORM\OneToOne(targetEntity="App\Entity\Adresse\Adresse", inversedBy="coordonnees")
     */
    private $adresse;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Contact\Web", inversedBy="coordonnees")
     */
    private $web;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contact\TelFixe", inversedBy="coordonnees")
     */
    private $telFixe;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contact\Portable", inversedBy="coordonnees")
     */
    private $portable;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contact\Fax", inversedBy="coordonnees")
     */
    private $fax;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\LieuDeFormation", mappedBy="coordonnees")
     */
    private $lieuFormation;


    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getLigne(): ?string
    {
        return $this->ligne;
    }

    public function setLigne(string $ligne): self
    {
        $this->ligne = $ligne;

        return $this;
    }

    public function getCouriel(): ?string
    {
        return $this->couriel;
    }

    public function setCouriel(string $couriel): self
    {
        $this->couriel = $couriel;

        return $this;
    }

    public function getContactFormation(): ?ContactFormation
    {
        return $this->contactFormation;
    }

    public function setContactFormation(?ContactFormation $contactFormation): self
    {
        $this->contactFormation = $contactFormation;

        return $this;
    }

    public function getOrganismeFormateur(): ?OrganismeFormateur
    {
        return $this->organismeFormateur;
    }

    public function setOrganismeFormateur(?OrganismeFormateur $organismeFormateur): self
    {
        $this->organismeFormateur = $organismeFormateur;

        return $this;
    }

    public function getContactOrganisme(): ?CoordonneesOrganisme
    {
        return $this->contactOrganisme;
    }

    public function setContactOrganisme(?CoordonneesOrganisme $contactOrganisme): self
    {
        $this->contactOrganisme = $contactOrganisme;

        return $this;
    }

    public function getAdresse(): ?Adresse
    {
        return $this->adresse;
    }

    public function setAdresse(?Adresse $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getWeb(): ?Web
    {
        return $this->web;
    }

    public function setWeb(?Web $web): self
    {
        $this->web = $web;

        return $this;
    }

    public function getTelFixe(): ?TelFixe
    {
        return $this->telFixe;
    }

    public function setTelFixe(?TelFixe $telFixe): self
    {
        $this->telFixe = $telFixe;

        return $this;
    }

    public function getPortable(): ?Portable
    {
        return $this->portable;
    }

    public function setPortable(?Portable $portable): self
    {
        $this->portable = $portable;

        return $this;
    }

    public function getFax(): ?Fax
    {
        return $this->fax;
    }

    public function setFax(?Fax $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getLieuFormation(): ?LieuDeFormation
    {
        return $this->lieuFormation;
    }

    public function setLieuFormation(?LieuDeFormation $lieuFormation): self
    {
        $this->lieuFormation = $lieuFormation;

        return $this;
    }
}
