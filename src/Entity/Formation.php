<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\Dictionnaire\DictAIS;
use App\Entity\Dictionnaire\DictTypeNiveaux;
use App\Entity\Dictionnaire\DictTypeParcours;
use App\Entity\Dictionnaire\DictTypePositionnement;
use App\Entity\Module\ModulePrerequis;
use App\Entity\Module\SousModules;
use App\Entity\Organisme\OrganismeFormationResponsable;
use App\Entity\Url\UrlFormation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;
use App\Entity\Certification;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\FormationRepository")
 */
class Formation extends BasePropriete
{
    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=255)
     */
    private $intituleFormation;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=3000)
     */
    private $objectifFormation;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=3000)
     */
    private $resultatsAttendus;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=3000)
     */
    private $contenuFormation;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=255)
     */
    private $identificationModule;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="integer")
     */
    private $certifiante;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="integer")
     */
    private $objectifGeneralFormaion;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="integer")
     */
    private $parcoursDeFormation;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="integer")
     */
    private $positionnement;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="integer")
     */
    private $codeNiveauEntree;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="integer")
     */
    private $codeNiveauSortie;

    /**
     * @Groups({"read", "write"})
     * @ORM\OneToMany(targetEntity="App\Entity\Action", mappedBy="formation")
     */
    private $actions;

    /**
     * @Groups({"read", "write"})
     * @ORM\OneToOne(targetEntity="App\Entity\Module\ModulePrerequis", mappedBy="formation")
     */
    private $modulePrerequis;

    /**
     * @Groups({"read", "write"})
     * @ORM\OneToOne(targetEntity="App\Entity\Module\SousModules", mappedBy="formation")
     */
    private $sousModules;

    /**
     * @Groups({"read", "write"})
     * @ORM\OneToOne(targetEntity="App\Entity\Dictionnaire\DictAIS")
     *
     * @var int
     */
    private $ais;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dictionnaire\DictTypeParcours")
     *
     * @var int
     */
    private $typeParcours;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dictionnaire\DictTypePositionnement")
     *
     * @var int
     */
    private $typePositionnement;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dictionnaire\DictTypeNiveaux")
     *
     * @var int
     */
    private $niveaux;

    /**
     * Une Formation contient plusieurs Certifications
     * 
     * @Groups({"read", "write"})
     * @ORM\OneToMany(targetEntity="App\Entity\Certification", mappedBy="formation")
     *
     * @var ArrayCollection<Certification>
     */
    private $certifications;

    /**
     * Une Formation possède 0 ou 1 URL de Formation
     * 
     * @Groups({"read", "write"})
     * @ORM\OneToOne(targetEntity="App\Entity\Url\UrlFormation", mappedBy="formation")
     *
     * @var int
     */
    private $urlFormation;

    /**
     * Une Formation possède 1 ou 0 Contact Formation
     * 
     * @Groups({"read", "write"})
     * @ORM\OneToOne(targetEntity="App\Entity\ContactFormation", mappedBy="formation", cascade={"persist"})
     *
     * @var int
     */
    private $contactFormation;

    /**
     * Une Formation est liée à un Organisme Responsable
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme\OrganismeFormationResponsable", inversedBy="formations")
     *
     * @var int
     */
    private $organismeFormationResponsable;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DomaineFormation", inversedBy="formation")
     */
    private $domaineFormation;

    /**
     * Une Formation possède plusieurs offres
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Offres", mappedBy="formation")
     *
     * @var ArrayCollection<Offres>
     */
    private $offres;


    public function __construct()
    {
        $this->certifications = new ArrayCollection();
        $this->actions = new ArrayCollection();
        $this->offres = new ArrayCollection();
    }


    public function getIntituleFormation(): ?string
    {
        return $this->intituleFormation;
    }

    public function setIntituleFormation(string $intituleFormation): self
    {
        $this->intituleFormation = $intituleFormation;

        return $this;
    }

    public function getObjectifFormation(): ?string
    {
        return $this->objectifFormation;
    }

    public function setObjectifFormation(string $objectifFormation): self
    {
        $this->objectifFormation = $objectifFormation;

        return $this;
    }

    public function getResultatsAttendus(): ?string
    {
        return $this->resultatsAttendus;
    }

    public function setResultatsAttendus(string $resultatsAttendus): self
    {
        $this->resultatsAttendus = $resultatsAttendus;

        return $this;
    }

    public function getContenuFormation(): ?string
    {
        return $this->contenuFormation;
    }

    public function setContenuFormation(string $contenuFormation): self
    {
        $this->contenuFormation = $contenuFormation;

        return $this;
    }

    public function getIdentificationModule(): ?string
    {
        return $this->identificationModule;
    }

    public function setIdentificationModule(string $identificationModule): self
    {
        $this->identificationModule = $identificationModule;

        return $this;
    }

    public function getCertifiante(): ?int
    {
        return $this->certifiante;
    }

    public function setCertifiante(int $certifiante): self
    {
        $this->certifiante = $certifiante;

        return $this;
    }

    public function getObjectifGeneralFormaion(): ?int
    {
        return $this->objectifGeneralFormaion;
    }

    public function setObjectifGeneralFormaion(int $objectifGeneralFormaion): self
    {
        $this->objectifGeneralFormaion = $objectifGeneralFormaion;

        return $this;
    }

    public function getParcoursDeFormation(): ?int
    {
        return $this->parcoursDeFormation;
    }

    public function setParcoursDeFormation(int $parcoursDeFormation): self
    {
        $this->parcoursDeFormation = $parcoursDeFormation;

        return $this;
    }

    public function getPositionnement(): ?int
    {
        return $this->positionnement;
    }

    public function setPositionnement(int $positionnement): self
    {
        $this->positionnement = $positionnement;

        return $this;
    }

    public function getCodeNiveauEntree(): ?int
    {
        return $this->codeNiveauEntree;
    }

    public function setCodeNiveauEntree(int $codeNiveauEntree): self
    {
        $this->codeNiveauEntree = $codeNiveauEntree;

        return $this;
    }

    public function getCodeNiveauSortie(): ?int
    {
        return $this->codeNiveauSortie;
    }

    public function setCodeNiveauSortie(int $codeNiveauSortie): self
    {
        $this->codeNiveauSortie = $codeNiveauSortie;

        return $this;
    }

    public function getAis(): ?DictAIS
    {
        return $this->ais;
    }

    public function setAis(?DictAIS $ais): self
    {
        $this->ais = $ais;

        return $this;
    }

    public function getTypeParcours(): ?DictTypeParcours
    {
        return $this->typeParcours;
    }

    public function setTypeParcours(?DictTypeParcours $typeParcours): self
    {
        $this->typeParcours = $typeParcours;

        return $this;
    }

    public function getTypePositionnement(): ?DictTypePositionnement
    {
        return $this->typePositionnement;
    }

    public function setTypePositionnement(?DictTypePositionnement $typePositionnement): self
    {
        $this->typePositionnement = $typePositionnement;

        return $this;
    }

    public function getNiveaux(): ?DictTypeNiveaux
    {
        return $this->niveaux;
    }

    public function setNiveaux(?DictTypeNiveaux $niveaux): self
    {
        $this->niveaux = $niveaux;

        return $this;
    }

    /**
     * @return Collection|Certification[]
     */
    public function getCertifications(): Collection
    {
        return $this->certifications;
    }

    public function addCertification(Certification $certification): self
    {
        if (!$this->certifications->contains($certification)) {
            $this->certifications[] = $certification;
            $certification->setFormation($this);
        }

        return $this;
    }

    public function removeCertification(Certification $certification): self
    {
        if ($this->certifications->contains($certification)) {
            $this->certifications->removeElement($certification);
            // set the owning side to null (unless already changed)
            if ($certification->getFormation() === $this) {
                $certification->setFormation(null);
            }
        }

        return $this;
    }

    public function getUrlFormation(): ?UrlFormation
    {
        return $this->urlFormation;
    }

    public function setUrlFormation(?UrlFormation $urlFormation): self
    {
        $this->urlFormation = $urlFormation;

        // set (or unset) the owning side of the relation if necessary
        $newFormation = $urlFormation === null ? null : $this;
        if ($newFormation !== $urlFormation->getFormation()) {
            $urlFormation->setFormation($newFormation);
        }

        return $this;
    }

    /**
     * @return Collection|Action[]
     */
    public function getActions(): Collection
    {
        return $this->actions;
    }

    public function addAction(Action $action): self
    {
        if (!$this->actions->contains($action)) {
            $this->actions[] = $action;
            $action->setFormations($this);
        }

        return $this;
    }

    public function removeAction(Action $action): self
    {
        if ($this->actions->contains($action)) {
            $this->actions->removeElement($action);
            // set the owning side to null (unless already changed)
            if ($action->getFormations() === $this) {
                $action->setFormations(null);
            }
        }

        return $this;
    }

    public function getContactFormation(): ?ContactFormation
    {
        return $this->contactFormation;
    }

    public function setContactFormation(?ContactFormation $contactFormation): self
    {
        $this->contactFormation = $contactFormation;

        // set (or unset) the owning side of the relation if necessary
        $newFormation = $contactFormation === null ? null : $this;
        if ($newFormation !== $contactFormation->getFormation()) {
            $contactFormation->setFormation($newFormation);
        }

        return $this;
    }

    public function getOrganismeFormationResponsable(): ?OrganismeFormationResponsable
    {
        return $this->organismeFormationResponsable;
    }

    public function setOrganismeFormationResponsable(?OrganismeFormationResponsable $organismeFormationResponsable): self
    {
        $this->organismeFormationResponsable = $organismeFormationResponsable;

        return $this;
    }

    public function getModulePrerequis(): ?ModulePrerequis
    {
        return $this->modulePrerequis;
    }

    public function setModulePrerequis(?ModulePrerequis $modulePrerequis): self
    {
        $this->modulePrerequis = $modulePrerequis;

        // set (or unset) the owning side of the relation if necessary
        $newFormation = $modulePrerequis === null ? null : $this;
        if ($newFormation !== $modulePrerequis->getFormation()) {
            $modulePrerequis->setFormation($newFormation);
        }

        return $this;
    }

    public function getSousModules(): ?SousModules
    {
        return $this->sousModules;
    }

    public function setSousModules(?SousModules $sousModules): self
    {
        $this->sousModules = $sousModules;

        // set (or unset) the owning side of the relation if necessary
        $newFormation = $sousModules === null ? null : $this;
        if ($newFormation !== $sousModules->getFormation()) {
            $sousModules->setFormation($newFormation);
        }

        return $this;
    }

    public function getDomaineFormation(): ?DomaineFormation
    {
        return $this->domaineFormation;
    }

    public function setDomaineFormation(?DomaineFormation $domaineFormation): self
    {
        $this->domaineFormation = $domaineFormation;

        return $this;
    }

    /**
     * @return Collection|Offres[]
     */
    public function getOffres(): Collection
    {
        return $this->offres;
    }

    public function addOffre(Offres $offre): self
    {
        if (!$this->offres->contains($offre)) {
            $this->offres[] = $offre;
            $offre->setFormation($this);
        }

        return $this;
    }

    public function removeOffre(Offres $offre): self
    {
        if ($this->offres->contains($offre)) {
            $this->offres->removeElement($offre);
            // set the owning side to null (unless already changed)
            if ($offre->getFormation() === $this) {
                $offre->setFormation(null);
            }
        }

        return $this;
    }
}
