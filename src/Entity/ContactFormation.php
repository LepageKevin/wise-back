<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactFormationRepository")
 */
class ContactFormation extends BasePropriete
{
    /**
     * Un ContactFormation est lié à une formation
     * 
     * @ORM\OneToOne(targetEntity="App\Entity\Formation", inversedBy="contactFormation")
     *
     * @var int
     */
    private $formation;

    /**
     * Un ContactFormation possède des Coordonnées
     * 
     * @Groups({"read", "write"})
     * @ORM\OneToOne(targetEntity="App\Entity\Coordonnees", mappedBy="contactFormation", cascade={"persist"})
     *
     * @var int
     */
    private $coordonnees;

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    public function getCoordonnees(): ?Coordonnees
    {
        return $this->coordonnees;
    }

    public function setCoordonnees(?Coordonnees $coordonnees): self
    {
        $this->coordonnees = $coordonnees;

        // set (or unset) the owning side of the relation if necessary
        $newContactFormation = $coordonnees === null ? null : $this;
        if ($newContactFormation !== $coordonnees->getContactFormation()) {
            $coordonnees->setContactFormation($newContactFormation);
        }

        return $this;
    }
}
