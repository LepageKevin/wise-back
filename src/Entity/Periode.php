<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\PeriodeRepository")
 */
class Periode extends BasePropriete
{
    /**
     * @ORM\Column(type="date")
     */
    private $debut;

    /**
     * @ORM\Column(type="date")
     */
    private $fin;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PeriodeInscription", mappedBy="periode")
     */
    private $periodeInscription;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Session", inversedBy="periode")
     */
    private $session;

    public function getDebut(): ?\DateTimeInterface
    {
        return $this->debut;
    }

    public function setDebut(\DateTimeInterface $debut): self
    {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): ?\DateTimeInterface
    {
        return $this->fin;
    }

    public function setFin(\DateTimeInterface $fin): self
    {
        $this->fin = $fin;

        return $this;
    }

    public function getPeriodeInscription(): ?PeriodeInscription
    {
        return $this->periodeInscription;
    }

    public function setPeriodeInscription(?PeriodeInscription $periodeInscription): self
    {
        $this->periodeInscription = $periodeInscription;

        // set (or unset) the owning side of the relation if necessary
        $newPeriode = $periodeInscription === null ? null : $this;
        if ($newPeriode !== $periodeInscription->getPeriode()) {
            $periodeInscription->setPeriode($newPeriode);
        }

        return $this;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }
}
