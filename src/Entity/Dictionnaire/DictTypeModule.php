<?php

namespace App\Entity\Dictionnaire;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Dictionnaire\Dictionnaire;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Dictionnaire\DictTypeModuleRepository")
 * @ORM\Table(name="dict_type_module")
 */
class DictTypeModule extends Dictionnaire
{
    
}
