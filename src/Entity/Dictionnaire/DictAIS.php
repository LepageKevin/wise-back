<?php

namespace App\Entity\Dictionnaire;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Dictionnaire\Dictionnaire;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Dictionnaire\DictAISRepository")
 * @ORM\Table(name="dict_ais")
 */
class DictAIS extends Dictionnaire
{

}
