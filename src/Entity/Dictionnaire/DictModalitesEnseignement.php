<?php

namespace App\Entity\Dictionnaire;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Dictionnaire\Dictionnaire;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Dictionnaire\DictModalitesEnseignementRepository")
 * @ORM\Table(name="dict_modalites_enseignement")
 */
class DictModalitesEnseignement extends Dictionnaire
{
  
}
