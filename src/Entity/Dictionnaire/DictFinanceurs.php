<?php

namespace App\Entity\Dictionnaire;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Dictionnaire\Dictionnaire;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Dictionnaire\DictFinanceursRepository")
 * @ORM\Table(name="dict_financeurs")
 */
class DictFinanceurs extends Dictionnaire
{

}
