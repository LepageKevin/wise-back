<?php

namespace App\Entity\Dictionnaire;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classe de Base des Dictionnaires
 * 
 * @ORM\MappedSuperclass
 * 
 * @abstract
 */
abstract class Dictionnaire
{
    /**
     * ID du Dictionnaire
     * 
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * 
     * @var integer
     */
    protected $id;

    /**
     * Clef du Dictionnaire
     * 
     * @ORM\Column(type="integer")
     * 
     * @var integer
     */
    protected $clef;

    /**
     * Valeur du Dictionnaire
     * 
     * @ORM\Column(type="string", nullable=false)
     *
     * @var string
     */
    protected $valeur;


    /**
     * Dictionnaire Constructeur
     * 
     * @param mixed|null $valeur Valeur du Dictionnaire (optionnel)
     * @param mixed|null $clef   Clef du Dictionnaire (optionnel)
     */
    public function __construct($valeur = null, $clef = null)
    {
        if ($valeur !== null)
            $this->setValeur($valeur);

        if ($clef !== null)
            $this->setClef($clef);
    }


    /**
     * Récupère l'ID du Dictionnaire
     *
     * @return integer
     */
    public function getID(): int
    {
        return $this->id;
    }


    /**
     * Modifier la clef du Dictionnaire
     *
     * @param integer $clef
     * 
     * @return self
     */
    public function setClef(int $clef): self
    {
        $this->clef = $clef;

        return $this;
    }


    /**
     * Récupère la Clef du Dictionnaire
     *
     * @return int
     */
    public function getClef(): int
    {
        return $this->clef;
    }


    /**
     * Modifie la Valeur du Dictionnaire
     *
     * @param string $valeur
     * 
     * @return self
     */
    public function setValeur(string $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }


    /**
     * Récupère la Valeur du Dictionnaire
     *
     * @return string|null
     */
    public function getValeur(): ?string
    {
        return $this->valeur;
    }
}