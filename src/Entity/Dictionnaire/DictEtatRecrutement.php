<?php

namespace App\Entity\Dictionnaire;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Dictionnaire\Dictionnaire;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Dictionnaire\DictEtatRecrutementRepository")
 * @ORM\Table(name="dict_etat_recrutement")
 */
class DictEtatRecrutement extends Dictionnaire
{
    
}
