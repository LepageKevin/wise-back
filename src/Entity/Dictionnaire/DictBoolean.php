<?php

namespace App\Entity\Dictionnaire;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Dictionnaire\Dictionnaire;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Dictionnaire\DictBooleanRepository")
 * @ORM\Table(name="dict_boolean")
 */
class DictBoolean extends Dictionnaire
{
}
