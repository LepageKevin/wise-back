<?php

namespace App\Entity\Contact;

use App\Entity\Coordonnees;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Contact\Contact;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Contact\TelFixeRepository")
 */
class TelFixe extends Contact
{
    /**
     * Un TelFixe est lié à 1 ou plusieurs Numéro de Tel
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Contact\Numtel", mappedBy="telFixe")
     *
     * @var ArrayCollection<Numtel>
     */
    private $nums;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Coordonnees", mappedBy="telFixe")
     */
    private $coordonnees;

    public function __construct()
    {
        $this->nums = new ArrayCollection();
        $this->coordonnees = new ArrayCollection();
    }

    /**
     * @return Collection|Numtel[]
     */
    public function getNums(): Collection
    {
        return $this->nums;
    }

    public function addNum(Numtel $num): self
    {
        if (!$this->nums->contains($num)) {
            $this->nums[] = $num;
            $num->setTelFixe($this);
        }

        return $this;
    }

    public function removeNum(Numtel $num): self
    {
        if ($this->nums->contains($num)) {
            $this->nums->removeElement($num);
            // set the owning side to null (unless already changed)
            if ($num->getTelFixe() === $this) {
                $num->setTelFixe(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Coordonnees[]
     */
    public function getCoordonnees(): Collection
    {
        return $this->coordonnees;
    }

    public function addCoordonnee(Coordonnees $coordonnee): self
    {
        if (!$this->coordonnees->contains($coordonnee)) {
            $this->coordonnees[] = $coordonnee;
            $coordonnee->setTelfixe($this);
        }

        return $this;
    }

    public function removeCoordonnee(Coordonnees $coordonnee): self
    {
        if ($this->coordonnees->contains($coordonnee)) {
            $this->coordonnees->removeElement($coordonnee);
            // set the owning side to null (unless already changed)
            if ($coordonnee->getTelfixe() === $this) {
                $coordonnee->setTelfixe(null);
            }
        }

        return $this;
    }
}
