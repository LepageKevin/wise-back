<?php

namespace App\Entity\Contact;

use App\Entity\Coordonnees;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Contact\Contact;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Contact\FaxRepository")
 */
class Fax extends Contact
{
    /**
     * Un Fax est liés à 1 ou plusieurs Numéro
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Contact\Numtel", mappedBy="fax")
     *
     * @var ArrayCollection<Numtel>
     */
    private $nums;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Coordonnees", mappedBy="fax")
     */
    private $coordonnees;

    public function __construct()
    {
        $this->nums = new ArrayCollection();
        $this->coordonnees = new ArrayCollection();
    }

    /**
     * @return Collection|Numtel[]
     */
    public function getNums(): Collection
    {
        return $this->nums;
    }

    public function addNum(Numtel $num): self
    {
        if (!$this->nums->contains($num)) {
            $this->nums[] = $num;
            $num->setFax($this);
        }

        return $this;
    }

    public function removeNum(Numtel $num): self
    {
        if ($this->nums->contains($num)) {
            $this->nums->removeElement($num);
            // set the owning side to null (unless already changed)
            if ($num->getFax() === $this) {
                $num->setFax(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Coordonnees[]
     */
    public function getCoordonnees(): Collection
    {
        return $this->coordonnees;
    }

    public function addCoordonnee(Coordonnees $coordonnee): self
    {
        if (!$this->coordonnees->contains($coordonnee)) {
            $this->coordonnees[] = $coordonnee;
            $coordonnee->setFax($this);
        }

        return $this;
    }

    public function removeCoordonnee(Coordonnees $coordonnee): self
    {
        if ($this->coordonnees->contains($coordonnee)) {
            $this->coordonnees->removeElement($coordonnee);
            // set the owning side to null (unless already changed)
            if ($coordonnee->getFax() === $this) {
                $coordonnee->setFax(null);
            }
        }

        return $this;
    }
}
