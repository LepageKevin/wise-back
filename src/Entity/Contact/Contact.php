<?php

namespace App\Entity\Contact;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * Classe de base des Contacts
 * 
 * @ORM\MappedSuperclass
 * 
 * @abstract
 */
abstract class Contact extends BasePropriete
{
    /**
     * Numéro Téléphone
     * 
     * @ORM\Column(type="integer", length=25)
     *
     * @var int
     */
    protected $numTel;


    /**
     * Modifier le numéro de Téléphone
     *
     * @param integer $tel
     * 
     * @return self
     */
    public function setNumTel(int $tel): self
    {
        $this->numTel = $tel;

        return $this;
    }


    /**
     * Récupère le Numéro de Téléphone
     *
     * @return integer|null
     */
    public function getNumTel(): ?int
    {
        return $this->numTel;
    }
}