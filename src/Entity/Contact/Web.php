<?php

namespace App\Entity\Contact;

use App\Entity\Coordonnees;
use App\Entity\Url\UrlWeb;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Contact\WebRepository")
 */
class Web extends BasePropriete
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Coordonnees", mappedBy="web")
     */
    private $coordonnees;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Url\UrlWeb", mappedBy="web")
     */
    private $urlWeb;

    public function __construct()
    {
        $this->urlWeb = new ArrayCollection();
    }

    public function getCoordonnees(): ?Coordonnees
    {
        return $this->coordonnees;
    }

    public function setCoordonnees(?Coordonnees $coordonnees): self
    {
        $this->coordonnees = $coordonnees;

        // set (or unset) the owning side of the relation if necessary
        $newWeb = $coordonnees === null ? null : $this;
        if ($newWeb !== $coordonnees->getWeb()) {
            $coordonnees->setWeb($newWeb);
        }

        return $this;
    }

    /**
     * @return Collection|UrlWeb[]
     */
    public function getUrlWeb(): Collection
    {
        return $this->urlWeb;
    }

    public function addUrlWeb(UrlWeb $urlWeb): self
    {
        if (!$this->urlWeb->contains($urlWeb)) {
            $this->urlWeb[] = $urlWeb;
            $urlWeb->setWeb($this);
        }

        return $this;
    }

    public function removeUrlWeb(UrlWeb $urlWeb): self
    {
        if ($this->urlWeb->contains($urlWeb)) {
            $this->urlWeb->removeElement($urlWeb);
            // set the owning side to null (unless already changed)
            if ($urlWeb->getWeb() === $this) {
                $urlWeb->setWeb(null);
            }
        }

        return $this;
    }
}
