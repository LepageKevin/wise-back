<?php

namespace App\Entity\Contact;

use App\Entity\Coordonnees;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Contact\Contact;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Contact\PortableRepository")
 */
class Portable extends Contact
{
    /**
     * Un Portable est liés à 1 ou plusieurs Numéro de Tel
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Contact\Numtel", mappedBy="portable")
     *
     * @var ArrayCollection<Numtel>
     */
    private $nums;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Coordonnees", mappedBy="portable")
     */
    private $coordonnees;

    public function __construct()
    {
        $this->nums = new ArrayCollection();
        $this->coordonnees = new ArrayCollection();
    }

    /**
     * @return Collection|Numtel[]
     */
    public function getNums(): Collection
    {
        return $this->nums;
    }

    public function addNum(Numtel $num): self
    {
        if (!$this->nums->contains($num)) {
            $this->nums[] = $num;
            $num->setPortable($this);
        }

        return $this;
    }

    public function removeNum(Numtel $num): self
    {
        if ($this->nums->contains($num)) {
            $this->nums->removeElement($num);
            // set the owning side to null (unless already changed)
            if ($num->getPortable() === $this) {
                $num->setPortable(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Coordonnees[]
     */
    public function getCoordonnees(): Collection
    {
        return $this->coordonnees;
    }

    public function addCoordonnee(Coordonnees $coordonnee): self
    {
        if (!$this->coordonnees->contains($coordonnee)) {
            $this->coordonnees[] = $coordonnee;
            $coordonnee->setPortable($this);
        }

        return $this;
    }

    public function removeCoordonnee(Coordonnees $coordonnee): self
    {
        if ($this->coordonnees->contains($coordonnee)) {
            $this->coordonnees->removeElement($coordonnee);
            // set the owning side to null (unless already changed)
            if ($coordonnee->getPortable() === $this) {
                $coordonnee->setPortable(null);
            }
        }

        return $this;
    }
}
