<?php

namespace App\Entity\Contact;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Contact\NumtelRepository")
 */
class Numtel extends BasePropriete
{
    /**
     * @ORM\Column(type="string", length=35)
     */
    private $value;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $type;

    /**
     * Un Numéro de Tel peut être lié à un Portable
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Contact\Portable", inversedBy="nums")
     *
     * @var int
     */
    private $portable;

    /**
     * Un Numéro peut être lié à un Fax
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Contact\Fax", inversedBy="nums")
     *
     * @var int
     */
    private $fax;

    /**
     * Un Numéro peut être lié à un TelFixe
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Contact\TelFixe", inversedBy="nums")
     *
     * @var int
     */
    private $telFixe;


    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPortable(): ?Portable
    {
        return $this->portable;
    }

    public function setPortable(?Portable $portable): self
    {
        $this->portable = $portable;

        return $this;
    }

    public function getFax(): ?Fax
    {
        return $this->fax;
    }

    public function setFax(?Fax $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getTelFixe(): ?TelFixe
    {
        return $this->telFixe;
    }

    public function setTelFixe(?TelFixe $telFixe): self
    {
        $this->telFixe = $telFixe;

        return $this;
    }
}
