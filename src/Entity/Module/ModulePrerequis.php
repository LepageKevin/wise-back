<?php

namespace App\Entity\Module;

use App\Entity\Formation;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Module\ModulePrerequisRepository")
 */
class ModulePrerequis extends BasePropriete
{
    /**
     * @ORM\Column(type="string", length=3000)
     */
    private $referenceModule;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Formation", inversedBy="modulePrerequis")
     */
    private $formation;

    public function getReferenceModule(): ?string {
        return $this->referenceModule;
    }

    public function setReferenceModule(string $referenceModule): self {
        $this->referenceModule=$referenceModule;

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->Formation;
    }

    public function setFormation(?Formation $Formation): self
    {
        $this->Formation = $Formation;

        return $this;
    }
}
