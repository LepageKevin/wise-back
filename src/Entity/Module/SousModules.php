<?php

namespace App\Entity\Module;

use App\Entity\Formation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Module\SousModulesRepository")
 */
class SousModules extends BasePropriete {

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Module\SousModule", mappedBy="sousModules")
     */
    private $sousModule;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Formation", inversedBy="sousModules")
     */
    private $formation;

    public function __construct()
    {
        $this->sousModule = new ArrayCollection();
    }

    /**
     * @return Collection|SousModule[]
     */
    public function getSousModule(): Collection
    {
        return $this->sousModule;
    }

    public function addSousModule(SousModule $sousModule): self
    {
        if (!$this->sousModule->contains($sousModule)) {
            $this->sousModule[] = $sousModule;
            $sousModule->setSousModules($this);
        }

        return $this;
    }

    public function removeSousModule(SousModule $sousModule): self
    {
        if ($this->sousModule->contains($sousModule)) {
            $this->sousModule->removeElement($sousModule);
            // set the owning side to null (unless already changed)
            if ($sousModule->getSousModules() === $this) {
                $sousModule->setSousModules(null);
            }
        }

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }
}
