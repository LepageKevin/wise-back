<?php

namespace App\Entity\Module;

use App\Entity\Dictionnaire\DictTypeModule;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Module\SousModuleRepository")
 */
class SousModule extends BasePropriete {
    /**
     * @ORM\Column(type="string", length=3000)
     */
    private $referenceModule;
    /**
     * @ORM\Column(type="integer")
     */
    private $typeModule;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Module\SousModules", inversedBy="sousModule")
     */
    private $sousModules;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dictionnaire\DictTypeModule")
     */
    private $dictTypeModule;

    public function getReferenceModule(): ?string {
        return $this->referenceModule;
    }

    public function setReferenceModule(string $referenceModule): self {
        $this->referenceModule=$referenceModule;

        return $this;
    }

    public function getTypeModule(): ?int {
        return $this->typeModule;
    }

    public function setTypeModule(int $typeModule): self {
        $this->typeModule=$typeModule;

        return $this;
    }

    public function getSousModules(): ?SousModules
    {
        return $this->sousModules;
    }

    public function setSousModules(?SousModules $sousModules): self
    {
        $this->sousModules = $sousModules;

        return $this;
    }

    public function getDictTypeModule(): ?DictTypeModule
    {
        return $this->dictTypeModule;
    }

    public function setDictTypeModule(?DictTypeModule $dictTypeModule): self
    {
        $this->dictTypeModule = $dictTypeModule;

        return $this;
    }
}
