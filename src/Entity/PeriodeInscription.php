<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PeriodeInscriptionRepository")
 */
class PeriodeInscription extends BasePropriete
{

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Periode", inversedBy="periodeInscription")
     */
    private $periode;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Session", inversedBy="periodeInscription")
     */
    private $session;

    public function getPeriode(): ?Periode
    {
        return $this->periode;
    }

    public function setPeriode(?Periode $periode): self
    {
        $this->periode = $periode;

        return $this;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }


}
