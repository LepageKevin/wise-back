<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;
/**
 * @ORM\Entity(repositoryClass="App\Repository\DateInformationRepository")
 */
class DateInformation extends BasePropriete
{
    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Action", inversedBy="dateInformation")
     */
    private $action;

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getAction(): ?Action
    {
        return $this->action;
    }

    public function setAction(?Action $action): self
    {
        $this->action = $action;

        return $this;
    }
}
