<?php

namespace App\Entity\Organisme;

use App\Entity\Action;
use App\Entity\BasePropriete;
use App\Entity\Dictionnaire\DictFinanceurs;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Organisme\OrganismeFinanceurRepository")
 */
class OrganismeFinanceur extends BasePropriete 
{
    /**
     * @ORM\Column(type="integer")
     */
    private $nbPlacesFinancees;

    /**
     * @ORM\Column(type="integer")
     */
    private $codeFinanceurs;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dictionnaire\DictFinanceurs")
     */
    private $dictFinanceurs;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Action", inversedBy="organismeFinanceur")
     */
    private $action;


    public function getNbPlacesFinancees(): ?int {
        return $this->nbPlacesFinancees;
    }

    public function setNbPlacesFinancees(int $nbPlacesFinancees): self {
        $this->nbPlacesFinancees=$nbPlacesFinancees;

        return $this;
    }

    public function getCodeFinanceurs(): ?int {
        return $this->codeFinanceurs;
    }

    public function setCodeFinanceurs(int $codeFinanceurs): self {
        $this->codeFinanceurs=$codeFinanceurs;

        return $this;
    }

    public function getDictFinanceurs(): ?DictFinanceurs
    {
        return $this->dictFinanceurs;
    }

    public function setDictFinanceurs(?DictFinanceurs $dictFinanceurs): self
    {
        $this->dictFinanceurs = $dictFinanceurs;

        // set (or unset) the owning side of the relation if necessary
        $newOrganismeFinanceur = $dictFinanceurs === null ? null : $this;
        if ($newOrganismeFinanceur !== $dictFinanceurs->getOrganismeFinanceur()) {
            $dictFinanceurs->setOrganismeFinanceur($newOrganismeFinanceur);
        }

        return $this;
    }

    public function getAction(): ?Action
    {
        return $this->action;
    }

    public function setAction(?Action $action): self
    {
        $this->action = $action;

        return $this;
    }
}
