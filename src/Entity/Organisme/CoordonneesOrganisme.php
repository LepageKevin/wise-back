<?php

namespace App\Entity\Organisme;

use App\Entity\Coordonnees;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Organisme\CoordonneesOrganismeRepository")
 */
class CoordonneesOrganisme extends BasePropriete
{
    /**
     * Organisme lié aux Coordonnées
     * 
     * @ORM\OneToOne(targetEntity="App\Entity\Organisme\OrganismeFormationResponsable", inversedBy="coordonnees")
     *
     * @var int
     */
    private $organisme;

    /**
     * Coordonnées liées aux CoordonnéesOrganisme
     * 
     * @ORM\OneToOne(targetEntity="App\Entity\Coordonnees", mappedBy="contactOrganisme")
     *
     * @var int
     */
    private $coordonnees;

    public function getOrganisme(): ?OrganismeFormationResponsable
    {
        return $this->organisme;
    }

    public function setOrganisme(?OrganismeFormationResponsable $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getCoordonnees(): ?Coordonnees
    {
        return $this->coordonnees;
    }

    public function setCoordonnees(?Coordonnees $coordonnees): self
    {
        $this->coordonnees = $coordonnees;

        // set (or unset) the owning side of the relation if necessary
        $newContactOrganisme = $coordonnees === null ? null : $this;
        if ($newContactOrganisme !== $coordonnees->getContactOrganisme()) {
            $coordonnees->setContactOrganisme($newContactOrganisme);
        }

        return $this;
    }
}
