<?php

namespace App\Entity\Organisme;

use App\Entity\Action;
use App\Entity\Coordonnees;
use App\Entity\Potentiel;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Organisme\OrganismeFormateurRepository")
 */
class OrganismeFormateur extends BasePropriete
{
    /**
     * @ORM\Column(type="string", length=14)
     */
    private $siret;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $raisonSocialFormateur;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Action", inversedBy="organismeFormateur")
     */
    private $action;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Potentiel", mappedBy="organismeFormateur")
     */
    private $potentiel;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Coordonnees", mappedBy="organismeFormateur")
     */
    private $coordonnees;

    public function getRaisonSocialFormateur(): ?string
    {
        return $this->raisonSocialFormateur;
    }

    public function setRaisonSocialFormateur(string $raisonSocialFormateur): self
    {
        $this->raisonSocialFormateur = $raisonSocialFormateur;

        return $this;
    }

    public function getAction(): ?Action
    {
        return $this->action;
    }

    public function setAction(?Action $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getPotentiel(): ?Potentiel
    {
        return $this->potentiel;
    }

    public function setPotentiel(?Potentiel $potentiel): self
    {
        $this->potentiel = $potentiel;

        // set (or unset) the owning side of the relation if necessary
        $newOrganismeFormateur = $potentiel === null ? null : $this;
        if ($newOrganismeFormateur !== $potentiel->getOrganismeFormateur()) {
            $potentiel->setOrganismeFormateur($newOrganismeFormateur);
        }

        return $this;
    }

    public function getCoordonnees(): ?Coordonnees
    {
        return $this->coordonnees;
    }

    public function setCoordonnees(?Coordonnees $coordonnees): self
    {
        $this->coordonnees = $coordonnees;

        // set (or unset) the owning side of the relation if necessary
        $newOrganismeFormateur = $coordonnees === null ? null : $this;
        if ($newOrganismeFormateur !== $coordonnees->getOrganismeFormateur()) {
            $coordonnees->setOrganismeFormateur($newOrganismeFormateur);
        }

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

}
