<?php

namespace App\Entity\Organisme;

use App\Entity\Formation;
use App\Entity\Potentiel;
use App\Entity\SiretOrganismeFormation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Organisme\OrganismeFormationResponsableRepository")
 */
class OrganismeFormationResponsable extends BasePropriete
{
    /**
     * @ORM\Column(type="string", length=14)
     */
    private $siret;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $raisonSocial;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomOrganisme;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private $numeroActivite;

    /**
     * @ORM\Column(type="string", length=3000)
     */
    private $renseignementSpecifique;

    /**
     * Coordonnees d'un Organisme
     * 
     * @ORM\OneToOne(targetEntity="App\Entity\Organisme\CoordonneesOrganisme", mappedBy="organisme")
     *
     * @var int
     */
    private $coordonnees;

    /**
     * Un Organisme de formation possède une ou plusieurs Formations
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Formation", mappedBy="organismeFormationResponsable")
     *
     * @var ArrayCollection<Formation>
     */
    private $formations;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Organisme\ContactOrganisme", inversedBy="organismeFormationResponsable")
     */
    private $contactOrganisme;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Potentiel", inversedBy="organismeFormationResponsables")
     */
    private $potentiels;
    

    public function __construct()
    {
        $this->formations = new ArrayCollection();
        $this->potentiels = new ArrayCollection();
    }


    public function getNomOrganisme(): ?string
    {
        return $this->nomOrganisme;
    }

    public function setNomOrganisme(string $nomOrganisme): self
    {
        $this->nomOrganisme = $nomOrganisme;

        return $this;
    }

    public function getNumeroActivite(): ?string
    {
        return $this->numeroActivite;
    }

    public function setNumeroActivite(string $numeroActivite): self
    {
        $this->numeroActivite = $numeroActivite;

        return $this;
    }

    public function getRenseignementSpecifique(): ?string
    {
        return $this->renseignementSpecifique;
    }

    public function setRenseignementSpecifique(string $renseignementSpecifique): self
    {
        $this->renseignementSpecifique = $renseignementSpecifique;

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
            $formation->setOrganismeFormationResponsable($this);
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        if ($this->formations->contains($formation)) {
            $this->formations->removeElement($formation);
            // set the owning side to null (unless already changed)
            if ($formation->getOrganismeFormationResponsable() === $this) {
                $formation->setOrganismeFormationResponsable(null);
            }
        }

        return $this;
    }

    public function getSiret(): ?SiretOrganismeFormation
    {
        return $this->siret;
    }

    public function setSiret(?SiretOrganismeFormation $siret): self
    {
        $this->siret = $siret;

        // set (or unset) the owning side of the relation if necessary
        $newOrganisme = $siret === null ? null : $this;
        if ($newOrganisme !== $siret->getOrganisme()) {
            $siret->setOrganisme($newOrganisme);
        }

        return $this;
    }

    public function getCoordonnees(): ?CoordonneesOrganisme
    {
        return $this->coordonnees;
    }

    public function setCoordonnees(?CoordonneesOrganisme $coordonnees): self
    {
        $this->coordonnees = $coordonnees;

        // set (or unset) the owning side of the relation if necessary
        $newOrganisme = $coordonnees === null ? null : $this;
        if ($newOrganisme !== $coordonnees->getOrganisme()) {
            $coordonnees->setOrganisme($newOrganisme);
        }

        return $this;
    }

    public function getContactOrganisme(): ?ContactOrganisme
    {
        return $this->contactOrganisme;
    }

    public function setContactOrganisme(?ContactOrganisme $contactOrganisme): self
    {
        $this->contactOrganisme = $contactOrganisme;

        return $this;
    }

    /**
     * @return Collection|Potentiel[]
     */
    public function getPotentiels(): Collection
    {
        return $this->potentiels;
    }

    public function addPotentiel(Potentiel $potentiel): self
    {
        if (!$this->potentiels->contains($potentiel)) {
            $this->potentiels[] = $potentiel;
        }

        return $this;
    }

    public function removePotentiel(Potentiel $potentiel): self
    {
        if ($this->potentiels->contains($potentiel)) {
            $this->potentiels->removeElement($potentiel);
        }

        return $this;
    }

    public function getRaisonSocial(): ?string
    {
        return $this->raisonSocial;
    }

    public function setRaisonSocial(string $raisonSocial): self
    {
        $this->raisonSocial = $raisonSocial;

        return $this;
    }

}
