<?php

namespace App\Entity\Organisme;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Organisme\ContactOrganismeRepository")
 */
class ContactOrganisme extends BasePropriete
{

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Organisme\OrganismeFormationResponsable", mappedBy="contactOrganisme")
     */
    private $organismeFormationResponsable;

    public function getOrganismeFormationResponsable(): ?OrganismeFormationResponsable
    {
        return $this->organismeFormationResponsable;
    }

    public function setOrganismeFormationResponsable(?OrganismeFormationResponsable $organismeFormationResponsable): self
    {
        $this->organismeFormationResponsable = $organismeFormationResponsable;

        // set (or unset) the owning side of the relation if necessary
        $newContactOrganisme = $organismeFormationResponsable === null ? null : $this;
        if ($newContactOrganisme !== $organismeFormationResponsable->getContactOrganisme()) {
            $organismeFormationResponsable->setContactOrganisme($newContactOrganisme);
        }

        return $this;
    }
    
}
