<?php

namespace App\Entity;

use App\Entity\Adresse\AdresseInscription;
use App\Entity\Dictionnaire\DictEtatRecrutement;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SessionRepository")
 */
class Session extends BasePropriete 
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $modalitesInscritpion;

    /**
     * @ORM\Column(type="integer")
     */
    private $etatRecrutement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Action", inversedBy="sessions")
     */
    private $action;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dictionnaire\DictEtatRecrutement")
     */
    private $dictEtatRecrutement;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PeriodeInscription", mappedBy="session")
     */
    private $periodeInscription;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Periode", mappedBy="session")
     */
    private $periode;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Adresse\AdresseInscription", mappedBy="session")
     */
    private $adresseInscription;
    

    public function getModalitesInscritpion(): ?string 
    {
        return $this->modalitesInscritpion;
    }


    public function setModalitesInscritpion(string $modalitesInscritpion): self 
    {
        $this->modalitesInscritpion=$modalitesInscritpion;

        return $this;
    }


    public function getEtatRecrutement(): ?int 
    {
        return $this->etatRecrutement;
    }


    public function setEtatRecrutement(int $etatRecrutement): self 
    {
        $this->etatRecrutement=$etatRecrutement;

        return $this;
    }

    public function getAction(): ?Action
    {
        return $this->action;
    }

    public function setAction(?Action $action): self
    {
        $this->action = $action;

        // set (or unset) the owning side of the relation if necessary
        $newSession = $action === null ? null : $this;
        if ($newSession !== $action->getSession()) {
            $action->setSession($newSession);
        }

        return $this;
    }

    public function getDictEtatRecrutement(): ?DictEtatRecrutement
    {
        return $this->dictEtatRecrutement;
    }

    public function setDictEtatRecrutement(?DictEtatRecrutement $dictEtatRecrutement): self
    {
        $this->dictEtatRecrutement = $dictEtatRecrutement;

        // set (or unset) the owning side of the relation if necessary
        $newSession = $dictEtatRecrutement === null ? null : $this;
        if ($newSession !== $dictEtatRecrutement->getSession()) {
            $dictEtatRecrutement->setSession($newSession);
        }

        return $this;
    }

    public function getPeriodeInscription(): ?PeriodeInscription
    {
        return $this->periodeInscription;
    }

    public function setPeriodeInscription(?PeriodeInscription $periodeInscription): self
    {
        $this->periodeInscription = $periodeInscription;

        // set (or unset) the owning side of the relation if necessary
        $newSession = $periodeInscription === null ? null : $this;
        if ($newSession !== $periodeInscription->getSession()) {
            $periodeInscription->setSession($newSession);
        }

        return $this;
    }

    public function getPeriode(): ?Periode
    {
        return $this->periode;
    }

    public function setPeriode(?Periode $periode): self
    {
        $this->periode = $periode;

        // set (or unset) the owning side of the relation if necessary
        $newSession = $periode === null ? null : $this;
        if ($newSession !== $periode->getSession()) {
            $periode->setSession($newSession);
        }

        return $this;
    }

    public function getAdresseInscription(): ?AdresseInscription
    {
        return $this->adresseInscription;
    }

    public function setAdresseInscription(?AdresseInscription $adresseInscription): self
    {
        $this->adresseInscription = $adresseInscription;

        // set (or unset) the owning side of the relation if necessary
        $newSession = $adresseInscription === null ? null : $this;
        if ($newSession !== $adresseInscription->getSession()) {
            $adresseInscription->setSession($newSession);
        }

        return $this;
    }
}
