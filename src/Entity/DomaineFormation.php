<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DomaineFormationRepository")
 */
class DomaineFormation extends BasePropriete
{
    /**
     * @ORM\Column(type="array", length=5)
     * 
     * @var array
     */
    private $codeFormacode = [];

    /**
     * @ORM\Column(type="array", length=3)
     * 
     * @var array
     */
    private $codeNSF = [];

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $codeRome;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Formation", mappedBy="domaineFormation")
     */
    private $formation;

    public function __construct()
    {
        $this->formation = new ArrayCollection();
    }


    public function getCodeFormacode(): array
    {
        return $this->codeFormacode;
    }

    public function setCodeFormacode(array $codeFormacode): self
    {
        $this->codeFormacode = $codeFormacode;

        return $this;
    }

    public function addCodeFormacode(string $codeFormacode): self
    {
        $this->codeFormacode[] = $codeFormacode;

        return $this;
    }

    public function getCodeNSF(): array
    {
        return $this->codeNSF;
    }

    public function setCodeNSF(array $codeNSF): self
    {
        $this->codeNSF = $codeNSF;

        return $this;
    }

    public function addCodeNSF(string $codeNSF): self
    {
        $this->codeNSF[] = $codeNSF;

        return $this;
    }

    public function getCodeRome(): ?string
    {
        return $this->codeRome;
    }

    public function setCodeRome(string $codeRome): self
    {
        $this->codeRome = $codeRome;

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormation(): Collection
    {
        return $this->formation;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->formation->contains($formation)) {
            $this->formation[] = $formation;
            $formation->setDomaineFormation($this);
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        if ($this->formation->contains($formation)) {
            $this->formation->removeElement($formation);
            // set the owning side to null (unless already changed)
            if ($formation->getDomaineFormation() === $this) {
                $formation->setDomaineFormation(null);
            }
        }

        return $this;
    }
}
