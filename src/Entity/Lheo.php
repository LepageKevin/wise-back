<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\Offres;

/**
 * @ApiResource(attributes={
 *     "normalization_context"={"groups"={"read"}},
 *     "denormalization_context"={"groups"={"write"}}
 * })
 * @ORM\Entity(repositoryClass="App\Repository\LheoRepository")
 */
class Lheo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Une donnée Lhéo contient une Offre
     * 
     * @Groups({"read", "write"})
     * @ORM\OneToOne(targetEntity="App\Entity\Offres", inversedBy="lheo")
     *
     * @var int
     */
    private $offre;
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOffre(): ?Offres
    {
        return $this->offre;
    }

    public function setOffre(?Offres $offre): self
    {
        $this->offre = $offre;

        // set (or unset) the owning side of the relation if necessary
        $newLheo = $offre === null ? null : $this;
        if ($newLheo !== $offre->getLheo()) {
            $offre->setLheo($newLheo);
        }

        return $this;
    }
}
