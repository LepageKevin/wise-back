<?php

namespace App\Entity\Url;

use App\Entity\Contact\Web;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Url\UrlWebRepository")
 */
class UrlWeb extends BasePropriete
{
    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=400)
     */
    private $urlWeb;

    /**
     * Un URL Web possède 1 ou 0 URL Formation
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Url\UrlFormation", inversedBy="urlWebs")
     *
     * @var int
     */
    private $urlFormation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contact\Web", inversedBy="urlWeb")
     */
    private $web;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Url\UrlAction", mappedBy="urlWeb")
     */
    private $urlAction;


    public function getUrlWeb(): ?string
    {
        return $this->urlWeb;
    }

    public function setUrlWeb(string $urlWeb): self
    {
        $this->urlWeb = $urlWeb;

        return $this;
    }

    public function getUrlFormation(): ?UrlFormation
    {
        return $this->urlFormation;
    }

    public function setUrlFormation(?UrlFormation $urlFormation): self
    {
        $this->urlFormation = $urlFormation;

        return $this;
    }

    public function getWeb(): ?Web
    {
        return $this->web;
    }

    public function setWeb(?Web $web): self
    {
        $this->web = $web;

        return $this;
    }

    public function getUrlAction(): ?UrlAction
    {
        return $this->urlAction;
    }

    public function setUrlAction(?UrlAction $urlAction): self
    {
        $this->urlAction = $urlAction;

        // set (or unset) the owning side of the relation if necessary
        $newUrlWeb = $urlAction === null ? null : $this;
        if ($newUrlWeb !== $urlAction->getUrlWeb()) {
            $urlAction->setUrlWeb($newUrlWeb);
        }

        return $this;
    }

}
