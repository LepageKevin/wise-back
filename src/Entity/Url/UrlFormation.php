<?php

namespace App\Entity\Url;

use App\Entity\Formation;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Url\UrlFormationRepository")
 */
class UrlFormation extends BasePropriete
{
    /**
     * Un URL Formation est lié à une Formation
     * 
     * @ORM\OneToOne(targetEntity="App\Entity\Formation", inversedBy="urlFormation")
     *
     * @var int
     */
    private $formation;

    /**
     * Un URL Formation possède un ou plusieurs URL Web
     * 
     * @Groups({"read", "write"})
     * @ORM\OneToMany(targetEntity="App\Entity\Url\UrlWeb", mappedBy="urlFormation")
     *
     * @var int
     */
    private $urlWebs;

    public function __construct()
    {
        $this->urlWebs = new ArrayCollection();
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    /**
     * @return Collection|UrlWeb[]
     */
    public function getUrlWebs(): Collection
    {
        return $this->urlWebs;
    }

    public function addUrlWeb(UrlWeb $urlWeb): self
    {
        if (!$this->urlWebs->contains($urlWeb)) {
            $this->urlWebs[] = $urlWeb;
            $urlWeb->setUrlFormation($this);
        }

        return $this;
    }

    public function removeUrlWeb(UrlWeb $urlWeb): self
    {
        if ($this->urlWebs->contains($urlWeb)) {
            $this->urlWebs->removeElement($urlWeb);
            // set the owning side to null (unless already changed)
            if ($urlWeb->getUrlFormation() === $this) {
                $urlWeb->setUrlFormation(null);
            }
        }

        return $this;
    }
}
