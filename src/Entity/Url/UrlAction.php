<?php

namespace App\Entity\Url;

use App\Entity\Action;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BasePropriete;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Url\UrlActionRepository")
 */
class UrlAction extends BasePropriete 
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Action", inversedBy="urlsAction")
     */
    private $action;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Url\UrlWeb", inversedBy="urlAction")
     */
    private $urlWeb;

    
    public function getAction(): ?Action
    {
        return $this->action;
    }

    public function setAction(?Action $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getUrlWeb(): ?UrlWeb
    {
        return $this->urlWeb;
    }

    public function setUrlWeb(?UrlWeb $urlWeb): self
    {
        $this->urlWeb = $urlWeb;

        return $this;
    }
}
