<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use \DateTime;

/**
 * Entité Utilisateur
 * 
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @UniqueEntity({"email"}, message="L'adresse Mail est déjà utilisée")
 * @UniqueEntity({"username"}, message="Le nom d'utilisateur est déjà utilisé")
 *
 * @category Entity
 */
class User implements UserInterface
{
    /**
     * ID Utilisateur
     * 
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * 
     * @var integer
     */
    private $id;

    /**
     * Nom Utilisateur
     * 
     * @ORM\Column(type="string", unique=true)
     * @Assert\Length(max=30, maxMessage="Le nom d'utilisateur doit être au maximum de {{ limit }} caractères.")
     * 
     * @var string
     */
    private $username;

    /**
     * Adresse Email de l'utilisateur
     * 
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email(message="L'email {{ value }} n'est pas une adresse valide.")
     * 
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * 
     * @var array
     */
    private $roles = [];

    /**
     * Mot de Passe
     * 
     * @var string The hashed password
     * 
     * @ORM\Column(type="string")
     * 
     * @var string
     */
    private $password;

    /**
     * Mot de Passe Clair
     * 
     * @Assert\Length(
     *      min=8, 
     *      max=255, 
     *          minMessage="Le mot de passe doit être au minimum de {{ limit }} caractères."
     *   )
     *
     * @var string
     */
    private $plainPassword;

    /**
     * Date de Création Utilisateur
     * 
     * @ORM\Column(type="datetime")
     *
     * @var DateTime
     */
    private $createdAt;


    /**
     * Constructeur
     * 
     * @final
     */
    final public function __construct()
    {
        $this->setRoles(['ROLE_USER']);
        $this->createdAt = new DateTime();
    }


    /**
     * Récupérer l'ID Utilisateur
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * Modifier le Nom Utilisateur
     *
     * @param string $username Nom d'utilisateur
     * 
     * @return self
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }


    /**
     * Récupérer le Nom Utilisateur
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }


    /**
     * Récupérer l'Adresse Email
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }


    /**
     * Modifier l'Adresse Email
     *
     * @param string $email Adresse Email
     * 
     * @return self
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
   

    /**
     * Récupérer les Rôles
     * 
     * @see UserInterface
     * 
     * @return array
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER'; //garantie d'avoir le Rôle 'USER' par défaut

        return \array_unique($roles);
    }


    /**
     * Modifier les Rôles
     *
     * @param array $roles Rôles
     * 
     * @return self
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }


    /**
     * Récupérer le Mot de Passe
     * 
     * @see UserInterface
     * 
     * @return string
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }


    /**
     * Modifier le Mot de Passe (encodé)
     *
     * @param string $password Mot de Passe (encodé)
     * 
     * @return self
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }


    /**
     * Récupérer le Mot de Passe Clair (non encodé / pas sauvegarder dans le BDD)
     *
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }


    /**
     * Modifier le Mot de Passe Clair
     *
     * @param string $password Mot de Passe Clair
     * 
     * @return self
     */
    public function setPlainPassword(string $password)
    {
        $this->plainPassword = $password;

        return $this;
    }


    /**
     * Récupérer la Date d'Inscription
     *
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }


    /**
     * Récupère le sel utilisé pour l'encodage
     * (pas nécessaire si on utilise l'algorythme "bcrypt")
     * 
     * @see UserInterface
     * 
     * @return string|void
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    
    /**
     * Efface les Identifiants Temporaires
     * 
     * @see UserInterface
     * 
     * @return void
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}