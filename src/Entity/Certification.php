<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\BasePropriete;
use App\Entity\Formation;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CertificationRepository")
 */
class Certification extends BasePropriete
{
    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=6)
     */
    private $codeRNCP;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=6)
     */
    private $codeCertifinfo;

    /**
     * Plusieurs Certifications on une seule Formation
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation", inversedBy="certifications")
     *
     * @var Formation
     */
    private $formation;


    public function getCodeRNCP(): ?string
    {
        return $this->codeRNCP;
    }

    public function setCodeRNCP(string $codeRNCP): self
    {
        $this->codeRNCP = $codeRNCP;

        return $this;
    }

    public function getCodeCertifinfo(): ?string
    {
        return $this->codeCertifinfo;
    }

    public function setCodeCertifinfo(string $codeCertifinfo): self
    {
        $this->codeCertifinfo = $codeCertifinfo;

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }
}
