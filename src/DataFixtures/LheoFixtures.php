<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Faker\Factory;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Adresse\Adresse;
use App\Entity\Adresse\AdresseInformation;
use App\Entity\Adresse\AdresseInscription;
use App\Entity\Contact\Contact;
use App\Entity\Contact\Fax;
use App\Entity\Contact\Numtel;
use App\Entity\Contact\Portable;
use App\Entity\Contact\TelFixe;
use App\Entity\Contact\Web;
use App\Entity\Module\ModulePrerequis;
use App\Entity\Module\SousModule;
use App\Entity\Module\SousModules;
use App\Entity\Organisme\ContactOrganisme;
use App\Entity\Organisme\CoordonneesOrganisme;
use App\Entity\Organisme\OrganismeFinanceur;
use App\Entity\Organisme\OrganismeFormateur;
use App\Entity\Organisme\OrganismeFormationResponsable;
use App\Entity\Url\UrlAction;
use App\Entity\Url\UrlFormation;
use App\Entity\Url\UrlWeb;
use App\Entity\Action;
use App\Entity\BasePropriete;
use App\Entity\Certification;
use App\Entity\ContactFormation;
use App\Entity\Coordonnees;
use App\Entity\DateInformation;
use App\Entity\DomaineInformation;
use App\Entity\Formation;
use App\Entity\Geolocalisation;
use App\Entity\Lheo;
use App\Entity\LieuDeFormation;
use App\Entity\Offres;
use App\Entity\Periode;
use App\Entity\PeriodeInscription;
use App\Entity\Potentiel;
use App\Entity\Session;
use App\Entity\SiretOrganismeFormation;

class LheoFixtures extends Fixture
{
    /**
     * Charge les Données dans le BDD
     *
     * @param ObjectManager $manager Une implémentation de ObjectManager
     * 
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        /*$faker = Factory::create();

        $adresse = new Adresse();
            $adresse->setLigne("30")
                ->setAttributs("attributs")
                ->setExtras("Extras")
                ->setCodePostal("49380")
                ->setVille("Angers")
                ->setDepartement("49")
                ->setCodeInseeCommune("10")
                ->setCodeInseeCanton("11")
                ->setRegion("49")
                ->setPays("fr");

            $manager->persist($adresse);

            $adresseInformation = new AdresseInformation();
            $adresseInformation->setAttributs("attributs")
                ->setExtras("Extras");

            $manager->persist($adresseInformation);


            $adresseInscription = new AdresseInscription();
            $adresseInscription->setAttributs("attributs")
                ->setExtras("Extras");

            $manager->persist($adresseInscription);

            $fax = new Fax();
            $fax->setNumTel("306987")
                ->setAttributs("attributs")
                ->setExtras("Extras");

            $manager->persist($fax);

            $numTel = new Numtel();
            $numTel->setValue("allo")
                ->setType("portable")
                ->setAttributs("attributs")
                ->setExtras("Extras");

            $manager->persist($numTel);

            $portable = new Portable();
            $portable->setNumTel("0654")
                ->setAttributs("attributs")
                ->setExtras("Extras");

            $manager->persist($portable);

            $telFixe = new TelFixe();
            $telFixe->setNumTel("065470")
                ->setAttributs("attributs")
                ->setExtras("Extras");

            $manager->persist($telFixe);

            $web = new Web();
            $web->setAttributs("attributs")
                ->setExtras("Extras");

            $manager->persist($web);

            $modulePrerequis = new ModulePrerequis();
            $modulePrerequis->setReferenceModule("referencemodule")
                ->setAttributs("attributs")
                ->setExtras("Extras");

            $manager->persist($modulePrerequis);

            $sousModule = new SousModule();
            $sousModule->setReferenceModule("referencemodule")
                ->setAttributs("attributs")
                ->setTypeModule("6")
                ->setExtras("Extras");

            $manager->persist($sousModule);

            $sousModules = new SousModules();
            $sousModules->setAttributs("attributs")
                ->setExtras("Extras");

            $manager->persist($sousModules);
            
        $manager->flush();*/
    }
}
