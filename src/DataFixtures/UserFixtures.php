<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Classe de Génération d'Utilisateurs Fake
 */
class UserFixtures extends Fixture
{
    /**
     * Encodeur de Mot de Passe
     *
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    

    /**
     * Constructeur
     * 
     * @param UserPasswordEncoderInterface $encoder Une implémentation de UserPasswordEncoderInterface (injectée)
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    /**
     * Charge les Données dans le BDD
     *
     * @param ObjectManager $manager Une implémentation de ObjectManager
     * 
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        //crée un administrateur
        $admin = $this->createAdmin("admin", "admin@admin.fr");
        $manager->persist($admin);

        //crée 5 utilisateurs Fake
        for ($i = 0; $i < 5; $i++)
        {
            $user = new User();
            $user->setUsername($faker->userName)
                ->setEmail($faker->email)
                ->setPassword($this->encoder->encodePassword($user, "P@ssword"));

            $manager->persist($user);
        }

        //pousse les utilisateurs dans la BDD
        $manager->flush();
    }


    /**
     * Crée un Administrateur dans le BDD
     * 
     * @param string $username Nom d'Utilisateur
     * @param string $email    Adresse Email
     *
     * @return User
     */
    private function createAdmin(string $username, string $email): User
    {
        $admin = new User();
        $admin->setUsername($username)
            ->setEmail($email)
            ->setPassword($this->encoder->encodePassword($admin, "admin"))
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN']);

        return $admin;
    }
}
