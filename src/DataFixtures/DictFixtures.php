<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Dictionnaire\DictAIS;
use App\Entity\Dictionnaire\DictBoolean;
use App\Entity\Dictionnaire\DictEtatRecrutement;
use App\Entity\Dictionnaire\DictFinanceurs;
use App\Entity\Dictionnaire\DictModalitesEnseignement;
use App\Entity\Dictionnaire\DictModalitesEs;
use App\Entity\Dictionnaire\DictPerimetreRecrutement;
use App\Entity\Dictionnaire\DictTypeModule;
use App\Entity\Dictionnaire\DictTypeNiveaux;
use App\Entity\Dictionnaire\DictTypeParcours;
use App\Entity\Dictionnaire\DictTypePositionnement;

/**
 * Génération des Données de Dictionnaires
 */
class DictFixtures extends Fixture
{
    /**
     * Manageur d'Entitées
     *
     * @var ObjectManager
     */
    private $manager;


    /**
     * DictFixtures Constructeur
     *
     * @param ObjectManager $manager Une implémentation de ObjectManager (injectée)
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }


    /**
     * Charge les Données
     *
     * @param ObjectManager $manager
     * 
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $this->loadDictBooleans();
        $this->loadDictModalitesEnseignements();
        $this->loadDictModalitesES();
        $this->loadDictEtatRecrutement();
        $this->loadDictAIS();
        $this->loadDictPerimetreRecrutement();
        $this->loadDictFinanceurs();
        $this->loadDictTypeParcours();
        $this->loadDictTypePositionnement();
        $this->loadDictTypeModule();
        $this->loadDictNiveaux();
    }


    /**
     * Flush un Dictionnaire
     *
     * @param array $dicts
     * 
     * @return void
     */
    private function flushDictionnaire(array $dicts)
    {
        foreach ($dicts as $dict) {
            $this->manager->persist($dict);
        }

        $this->manager->flush();
    }


    /**
     * Charge les Dictionnaires de Boolean
     * <dict-boolean>
     *
     * @return void
     */
    private function loadDictBooleans()
    {
        $bools = [
            new DictBoolean("non", 0),
            new DictBoolean("oui", 1)
        ];

        $this->flushDictionnaire($bools);
    }


    /**
     * Charge les Dictionnaires Modalites Enseignement
     * <dict-modalites-enseignement>
     *
     * @return void
     */
    private function loadDictModalitesEnseignements()
    {
        $modalitesEns = [
            new DictModalitesEnseignement("formation entièrement présentielle", 0),
            new DictModalitesEnseignement("formation mixte", 1),
            new DictModalitesEnseignement("formation entièrement à distance", 2)
        ];

        $this->flushDictionnaire($modalitesEns);
    }


    /**
     * Charge les Dictionnaires Modalites ES
     * <dict-modalites-es>
     *
     * @return void
     */
    private function loadDictModalitesES()
    {
        $modalitesES = [
            new DictModalitesEs("entrées/sorties à dates fixe", 0),
            new DictModalitesEs("entrées/sorties permanentes", 1)
        ];

        $this->flushDictionnaire($modalitesES);
    }


    /**
     * Charge les Dictionnaires d'Etat Recrutement
     * <dict-etat-recrutement>
     *
     * @return void
     */
    private function loadDictEtatRecrutement()
    {
        $etatsRecrutement = [
            new DictEtatRecrutement("ouvert", 1),
            new DictEtatRecrutement("fermé", 2),
            new DictEtatRecrutement("suspendu", 3)
        ];

        $this->flushDictionnaire($etatsRecrutement);
    }


    /**
     * Charge les Dictionnaires DictAis
     * <dict-AIS>
     *
     * @return void
     */
    private function loadDictAIS()
    {
        $ais = [
            new DictAIS("code(s) obselète(s)", 1),
            new DictAIS("Perfectionnement, élargissement des compétences", 2),
            new DictAIS("Perfectionnement, élargissement des compétences", 3),
            new DictAIS("Création d'entreprise", 4),
            new DictAIS("Remise à niveau, maîtrise des savoirs de base, initiation", 5),
            new DictAIS("Certification", 6),
            new DictAIS("Professionnalisation", 7),
            new DictAIS("Préparation à la qualification", 8),
            new DictAIS("(Re)mobilisation, aide à l'élaboration de projet professionnel", 9),
        ];

        $this->flushDictionnaire($ais);
    }


    /**
     * Charge les Dictionnaires Périmètres de Recrutement
     * <dict-perimetre-recrutement>
     *
     * @return void
     */
    private function loadDictPerimetreRecrutement()
    {
        $perimetres = [
            new DictPerimetreRecrutement("Autres", 0),
            new DictPerimetreRecrutement("Commune", 1),
            new DictPerimetreRecrutement("Département", 2),
            new DictPerimetreRecrutement("Région", 3),
            new DictPerimetreRecrutement("Interrégion", 4),
            new DictPerimetreRecrutement("Pays", 5),
            new DictPerimetreRecrutement("International", 6)
        ];

        $this->flushDictionnaire($perimetres);
    }


    /**
     * Charge les Dictionnaires Financeurs
     * <dict-financeurs>
     *
     * @return void
     */
    private function loadDictFinanceurs()
    {
        $financeurs = [
            new DictFinanceurs("Autres", 0),
            new DictFinanceurs("Code(s) obselète(s)", 1),
            new DictFinanceurs("Collectivité territoriale - Conseil régonial", 2),
            new DictFinanceurs("Fonds européens - FSE", 3),
            new DictFinanceurs("Pôle emploi", 4),
            new DictFinanceurs("Entreprise", 5),
            new DictFinanceurs("ACSÉ (anciennement FASILD)", 6),
            new DictFinanceurs("AGEFIPH", 7),
            new DictFinanceurs("Collectivité territoriale - Conseil général", 8),
            new DictFinanceurs("Collectivité territoriale - Commune", 9),
            new DictFinanceurs("Bénéficiaire de l'action", 10),
            new DictFinanceurs("Etat - Ministère chargé de l'emploi", 11),
            new DictFinanceurs("Etat - Ministère de l'éducation nationale", 12),
            new DictFinanceurs("Etat - Autre", 13),
            new DictFinanceurs("Fonds européens - Autre", 14),
            new DictFinanceurs("Collectivité territoriale - Autre", 15),
            new DictFinanceurs("OPCA", 16),
            new DictFinanceurs("OPACIF", 17)
        ];

        $this->flushDictionnaire($financeurs);
    }


    /**
     * Charge les Dictionnaires Type de Parcours 
     * <dict-type-parcours>
     *
     * @return void
     */
    private function loadDictTypeParcours()
    {
        $parcours = [
            new DictTypeParcours("en groupe (non personnalisable)", 1),
            new DictTypeParcours("individualisé", 2),
            new DictTypeParcours("modularisé", 3),
            new DictTypeParcours("mixte", 4)
        ];

        $this->flushDictionnaire($parcours);
    }


    /**
     * Charge les Dictionnaires Type de Positionnement
     * <dict-type-positionnement>
     *
     * @return void
     */
    private function loadDictTypePositionnement()
    {
        $positionnements = [
            new DictTypePositionnement("réglementaire", 1),
            new DictTypePositionnement("pédagogique", 2)
        ];

        $this->flushDictionnaire($positionnements);
    }


    /**
     * Charge les Dictionnaires Type de Module
     * <dict-type-module>
     *
     * @return void
     */
    private function loadDictTypeModule()
    {
        $modules = [
            new DictTypeModule("information inconnue", 0),
            new DictTypeModule("obligatoire", 1),
            new DictTypeModule("personnalisable", 2)
        ];

        $this->flushDictionnaire($modules);
    }


    /**
     * Charge les Dictionnaires de Niveaux
     * <dict-niveaux>
     *
     * @return void
     */
    private function loadDictNiveaux()
    {
        $niveaux = [
            new DictTypeNiveaux("information non communiquée", 0),
            new DictTypeNiveaux("sans niveau specifique", 1),
            new DictTypeNiveaux("niveau VI (illettrisme, analphabetisme)", 2),
            new DictTypeNiveaux("niveau V bis (prequalification)", 3),
            new DictTypeNiveaux("niveau V (CAP, BEP, CFPA du premier degré)", 4),
            new DictTypeNiveaux("niveau IV (BP, BT, baccalaureat professionnel ou technologique)", 5),
            new DictTypeNiveaux("niveau III (BTS, DUT)", 6),
            new DictTypeNiveaux("niveau II (licence ou maîtrise universitaire)", 7),
            new DictTypeNiveaux("niveau I (supérieur à la maîtrise)", 8)
        ];

        $this->flushDictionnaire($niveaux);
    }
}
