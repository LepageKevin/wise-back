<?php

namespace App\DataFixtures;

use App\Entity\Lheo;
use App\Entity\Offres;
use App\Entity\Periode;
use App\Entity\Formation;
use App\Entity\Coordonnees;
use App\Entity\ContactFormation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class OffreFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $lheo = new Lheo();
        $offre = new Offres();
        $formation = new Formation();
        $periode = new Periode();
        $contactFormation = new ContactFormation();
        $coordonnees = new Coordonnees();

        $coordonnees->setCivilite("Mr")
            ->setNom("Nom")
            ->setPrenom("Prenom")
            ->setLigne("1")
            ->setCouriel("couriel");

        $contactFormation->setCoordonnees($coordonnees);

        $periode->setDebut(new \DateTime())
            ->setFin(new \DateTime());

        $formation->setIntituleFormation("Formation de Test !")
            ->setObjectifFormation("Formation d'apprentissage à la pêche")
            ->setObjectifGeneralFormaion(2)
            ->setIdentificationModule("AIS")
            ->setCertifiante(2)
            ->setParcoursDeFormation(1)
            ->setPositionnement(1)
            ->setCodeNiveauEntree(1)
            ->setCodeNiveauSortie(2)
            ->setContenuFormation("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque consequat quis enim id faucibus. Mauris dictum sem dignissim, eleifend quam luctus, tristique magna.")
            ->setResultatsAttendus("Résulat attendus : Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque consequat quis enim id faucibus. Mauris dictum sem dignissim, eleifend quam luctus, tristique magna.")
            ->setContactFormation($contactFormation);

        $offre->setFormation($formation);
        $lheo->setOffre($offre);

        $manager->persist($lheo);
        $manager->persist($formation);
        $manager->persist($offre);
        $manager->persist($periode);

        $manager->flush();
    }
}
